# Documentation

## Requirements

* Python 3.7
* MySQL 8.0 or MariaDB 10.3

## Installation

1. Clone this repository

    ```
    git clone https://gitlab.com/ilos-vigil/wish-store-restful-api.git
    cd wish-store-restful-api/
    ```

2. Install and enable venv (optional, but recommended)

    ```
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install required library

    ```
    pip3 install --user --requirement requirements.txt
    ```

4. Create new database and user in MySQL/MariaDB

    ```sql
    CREATE DATABASE wishstore;
    create user 'wishstore'@'localhost' identified by 'Test123!';
    GRANT ALL PRIVILEGES ON wishstore.* TO 'wishstore'@'localhost';
    COMMIT;
    ```

5. Copy config.py from config.example.py

    ```
    cd src/
    cp config.example.py config.py
    ```

6. Modify config.py based on MySQL/MariaDB configuration

7. Perform database migration and seeding

    ```
    flask db init
    flask db migrate
    flask db upgrade
    flask seed
    ```

8. Start RESTful API on development mode

    ```
    python3 app.py
    ```

## Endpoint

Download and install [Insomnia REST Client](https://insomnia.rest/), then load `insomnia_docs.sjon`

## Directory Structure

```
wish-store-restful-api/
    app.py
    config.py
    error.py
    utils.py
    route/
    decorator/
    controller/
    model/
        seed/
    static/
```

* `app.py` : Base flask app configuration
* `error.py` : All custom error handling and custom exception located hero
* `config.py` : Flask configuration for different environment (development, deployment, etc.)
* `utils.py` : Function used by all model & decorator located in here
* `route/` : Contains all routes
  * `__init__.py` : All routes is initialized here
* `decorator/` : Contains all decorator
* `controller/` : Contains all controller
  * `__init__.py` : Base Class for most controller located in here
* `model/` : Contains all model and seed
  * `seed/` : Contains all seed
* `static/` : Contains default file & file uploaded for user

## Table

![](./db.png)

## Deployment

See [https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/)
