import base64
from model.user import User
from config import Development

WISH_DOMAIN = Development.SERVER_NAME

def base64_to_file(model, id, base64_string):
    '''
    Convert received Base64 into .jpg file
    '''
    base64_encode = bytes(base64_string, 'utf-8')
    base64_decode = base64.decodebytes(base64_encode)
    path = f'./static/image/{model}/{id}.jpg'
    file = open(path, 'wb')
    file.write(base64_decode)


def msg_model_not_exist(model_name):
    return f"Item {model_name} is not exist"


def get_user_id(**column):
    '''
    Get User ID from known token or username
    '''
    if 'token' in column:
        return User.get({'token': column['token']}).to_json()['id']
    elif 'username' in column:
        return User.get({'username': column['username']}).to_json()['id']
    else:
        return None
