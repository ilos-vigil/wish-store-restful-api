import utils
from sqlalchemy import func
from model import db
from error import *



class UserReview(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, nullable=True)
    id_target = db.Column(db.Integer, nullable=True)
    comment = db.Column(db.String(255), nullable=True)
    rating = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user_user = db.relationship('User', primaryjoin='foreign(UserReview.id_user) == User.id')
    user_target = db.relationship('User', primaryjoin='foreign(UserReview.id_target) == User.id')

    def __init__(self, json):
        self.id_user = json['created_by']
        self.id_target = json['id_target']
        self.comment = json['comment'] if 'comment' in json else None
        self.rating = json['rating']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_user': self.id_user,
            'user_user': self.user_user.to_json_min(),
            'id_target': self.id_target,
            'user_target': self.user_target.to_json_min(),
            'comment': self.comment,
            'rating': self.rating,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        user_review = None
        if json is None or len(json.keys()) == 0:
            user_review = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            user_review = cls.query.get(json['id'])
        elif 'id_target' in json:
            user_review = cls.query.filter_by(is_deleted=0, id_target=json['id_target']).all()
        else:
            raise InvalidGETKeyError

        if user_review is None or (type(user_review) is not list and user_review.is_deleted == 1):
            raise ItemNotExistError
        return user_review

    @classmethod
    def post(cls, json):
        user_review = UserReview(json)
        db.session.add(user_review)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            user_review = cls.query.get(json['id'])
            if user_review is None or user_review.is_deleted == 1:
                raise ItemNotExistError
            else:
                user_review.comment = json['comment'] if 'comment' in json else user_review.comment
                user_review.rating = json['rating'] if 'rating' in json else user_review.rating

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            user_review = cls.query.get(json['id'])
            if user_review is None or user_review.is_deleted == 1:
                raise ItemNotExistError
            else:
                user_review.is_deleted = 1
                user_review.deleted_by = json['deleted_by']
                user_review.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
