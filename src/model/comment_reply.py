import datetime
import utils
from sqlalchemy import func, and_
from model import db
from error import *


class CommentReply(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_comment = db.Column(db.Integer, nullable=True)
    pesan = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user = db.relationship('User', primaryjoin='foreign(CommentReply.created_by) == User.id')

    def __init__(self, json):
        self.id_comment = json['id_comment']
        self.pesan = json['pesan']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_comment': self.id_comment,
            'pesan': self.pesan,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'user': self.user.to_json_min(),
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        comment_reply = None
        if json is None or len(json.keys()) == 0:
            comment_reply = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            comment_reply = cls.query.get(json['id'])
        elif 'user_friends_id' in json:
            current_time = datetime.datetime.utcnow()
            three_days_ago = current_time - datetime.timedelta(days=3)
            comment_reply = db.session.query(CommentReply).filter(and_(CommentReply.created_by.in_(json['user_friends_id']), CommentReply.created_at > current_time - three_days_ago)).all()
        else:
            raise InvalidGETKeyError

        if comment_reply is None or (type(comment_reply) is not list and comment_reply.is_deleted == 1):
            raise ItemNotExistError
        return comment_reply

    @classmethod
    def post(cls, json):
        comment_reply = CommentReply(json)
        db.session.add(comment_reply)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            comment_reply = cls.query.get(json['id'])
            if comment_reply is None or comment_reply.is_deleted == 1:
                raise ItemNotExistError
            else:
                comment_reply.pesan = json['pesan'] if 'pesan' in json else comment_reply.pesan

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            comment_reply = cls.query.get(json['id'])
            if comment_reply is None or comment_reply.is_deleted == 1:
                raise ItemNotExistError
            else:
                comment_reply.is_deleted = 1
                comment_reply.deleted_by = json['deleted_by']
                comment_reply.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
