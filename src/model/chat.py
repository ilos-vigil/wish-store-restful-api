import utils
from sqlalchemy import func, and_
from model import db
from error import *

class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_sender = db.Column(db.Integer, nullable=True)
    id_receiver = db.Column(db.Integer, nullable=True)
    id_set = db.Column(db.String(255), nullable=True, index=True)
    message = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user_sender = db.relationship('User', primaryjoin='foreign(Chat.id_sender) == User.id')
    user_receiver = db.relationship('User', primaryjoin='foreign(Chat.id_receiver) == User.id')

    def __init__(self, json):
        self.id_sender = json['id_sender']
        self.id_receiver = json['id_receiver']
        self.message = json['message']
        self.created_by = json['created_by']
        if(self.id_sender < self.id_receiver):
            self.id_set = f'{self.id_sender}|{self.id_receiver}'
        else:
            self.id_set = f'{self.id_receiver}|{self.id_sender}'

    def to_json(self):
        return {
            'id': self.id,
            'id_sender': self.id_sender,
            'id_receiver': self.id_receiver,
            'id_set': self.id_set,
            'user_sender': self.user_sender.to_json_min(),
            'user_receiver': self.user_receiver.to_json_min(),
            'message': self.message,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        chat = None
        if json is None or len(json.keys()) == 0:
            chat = cls.query.filter_by(is_deleted=0).all()
        elif 'id_user' in json:
            chat_subquery = db.session.query(func.max(Chat.id).label('id_max')).filter(
                (Chat.id_sender == json['id_user']) | (Chat.id_receiver == json['id_user'])).group_by(Chat.id_set).subquery()
            chat = db.session.query(Chat).filter(Chat.id.in_(chat_subquery)).all()
        elif 'id_logon_user' in json and 'id_other_user' in json:
            chat = cls.query.filter(
                        (and_(cls.id_sender == json['id_logon_user'], cls.id_receiver == json['id_other_user'])) |
                        (and_(cls.id_sender == json['id_other_user'], cls.id_receiver == json['id_logon_user']))
                    ).order_by(cls.created_at).all()
        elif 'id' in json:
            chat = cls.query.get(json['id'])
        else:
            raise InvalidGETKeyError

        if chat is None or (type(chat) is not list and chat.is_deleted == 1):
            raise ItemNotExistError
        return chat


    @classmethod
    def post(cls, json):
        chat = Chat(json)
        db.session.add(chat)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            chat = cls.query.get(json['id'])
            if chat is None or chat.is_deleted == 1:
                raise ItemNotExistError
            else:
                chat.message = json['message'] if 'message' in json else chat.message

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            chat = cls.query.get(json['id'])
            if chat is None or chat.is_deleted == 1:
                raise ItemNotExistError
            else:
                chat.is_deleted = 1
                chat.deleted_by = json['deleted_by']
                chat.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
