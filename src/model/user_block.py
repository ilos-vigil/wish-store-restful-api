import utils
from sqlalchemy import func
from model import db
from error import *



class UserBlock(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, nullable=True)
    id_target = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    __table_args__ = (
        db.UniqueConstraint('id_user', 'id_target', name='constraint_user_target'),
    )

    def __init__(self, json):
        self.id_user = json['id_user']
        self.id_target = json['id_target']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_user': self.id_user,
            'id_target': self.id_target,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        user_block = None
        if json is None or len(json.keys()) == 0:
            user_block = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            user_block = cls.query.get(json['id'])
        else:
            raise InvalidGETKeyError

        if user_block is None or (type(user_block) is not list and user_block.is_deleted == 1):
            raise ItemNotExistError
        else:
            return user_block

    @classmethod
    def post(cls, json):
        user_block = UserBlock(json)
        db.session.add(user_block)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            user_block = cls.query.get(json['id'])
            if user_block is None or user_block.is_deleted == 1:
                raise ItemNotExistError
            else:
                user_block.is_accepted = json['is_accepted'] if 'is_accepted' in json else user_block.is_accepted

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            user_block = cls.query.get(json['id'])
            if user_block is None or user_block.is_deleted == 1:
                raise ItemNotExistError
            else:
                db.session.delete(user_block)
                db.session.commit()
        elif 'id_target' in json and 'id_user' in json:
            user_block = cls.query.filter_by(id_target=json['id_target'], id_user=json['id_user']).first()
            if user_block is None:
                raise ItemNotExistError
            else:
                db.session.delete(user_block)
                db.session.commit()
        else:
            raise MissingIDKeyError
