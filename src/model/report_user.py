import utils
from sqlalchemy import func
from model import db
from error import *



class ReportUser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_pelapor = db.Column(db.Integer, nullable=True)
    id_user = db.Column(db.Integer, nullable=True)
    keterangan = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user_pelapor = db.relationship('User', primaryjoin='foreign(ReportUser.id_pelapor) == User.id')
    user_dilapor = db.relationship('User', primaryjoin='foreign(ReportUser.id_user) == User.id')

    def __init__(self, json):
        self.id_pelapor = json['id_pelapor']
        self.id_user = json['id_user']
        self.keterangan = json['keterangan']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_pelapor': self.id_pelapor,
            'user_pelapor': self.user_pelapor.to_json(),
            'id_user': self.id_user,
            'user_dilapor': self.user_dilapor.to_json(),
            'keterangan': self.keterangan,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        report_user = None
        if json is None or len(json.keys()) == 0:
            report_user = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            report_user = cls.query.get(json['id'])
        else:
            raise InvalidGETKeyError
        
        if report_user is None or (type(report_user) is not list and report_user.is_deleted == 1):
            raise ItemNotExistError
        else:
            return report_user

    @classmethod
    def post(cls, json):
        report_user = ReportUser(json)
        db.session.add(report_user)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            report_user = cls.query.get(json['id'])
            if report_user is None or report_user.is_deleted == 1:
                raise ItemNotExistError
            else:
                report_user.keterangan = json['keterangan'] if 'keterangan' in json else report_user.keterangan

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            report_user = cls.query.get(json['id'])
            if report_user is None or report_user.is_deleted == 1:
                raise ItemNotExistError
            else:
                report_user.is_deleted = 1
                report_user.deleted_by = json['deleted_by']
                report_user.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
