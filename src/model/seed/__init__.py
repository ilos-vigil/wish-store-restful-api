import os
import json
from pathlib import Path
from model import (
    Category,
    Chat,
    CommentReply,
    Comment,
    Deal,
    Post,
    ReportPost,
    ReportUser,
    Trans,
    UserBlock,
    UserFriend,
    UserReview,
    User,
    Wishlist,
)


def seed_all():
    seed_mock()
    seed_category()
    seed_json_file()
    print("All seeds successfully created!")


def seed_mock():
    '''
    Example data for testing
    '''
    User.post({
        "id_onesignal": "123",
        "nama": "Bob",
        "gender": 0,
        "username": "bob",
        "password": "Test123!",
        "no_rekening": "123",
        "is_admin": 1,
        "token": "token777",
        "alamat": "NULL",
        "kota": "NULL",
        "created_by": None
    })
    User.post({
        "id_onesignal": "123",
        "nama": "Alice",
        "gender": 1,
        "username": "alice",
        "password": "Test123!",
        "no_rekening": "123",
        "is_admin": 0,
        "token": "token123",
        "alamat": "Ngagel 3",
        "kota": "Surabaya",
        "created_by": None
    })
    User.post({
        "id_onesignal": "123",
        "nama": "Mallory",
        "gender": 1,
        "username": "mallory",
        "password": "Test123!",
        "no_rekening": "123",
        "is_admin": 0,
        "token": "token666",
        "alamat": "alamat",
        "kota": "kota",
        "created_by": None
    })

    Category.post({
        "nama": "Elektronik",
        "created_by": 1
    })
    Category.post({
        "nama": "Perhiasan",
        "created_by": 1
    })
    Post.post({
        "judul": "Xiaomi Redmi Note 8",
        "deskripsi": "Mau HP baru gan!",
        "id_category": "1",
        "url_video": None,
        "user_alamat": None,
        "user_kota": None,
        "created_by": 2,
        "base64": "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEP\nERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4e\nHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAC4ALgDASIA\nAhEBAxEB/8QAHQAAAgIDAQEBAAAAAAAAAAAAAAcGCAEEBQMCCf/EAEoQAAEDAwIDBQUFAgkLBQEA\nAAECAwQFBhEABxIhMQgTQVFhFCIycYEVQlKRoSNyCRYkM2KCkqKxFzQ1Q1Njc7LB0fAlRJSkw9L/\nxAAbAQEAAgMBAQAAAAAAAAAAAAAAAwQBAgUHBv/EADIRAAIBAgQCCQMDBQAAAAAAAAABAgMRBAUh\nMRJBBjJRYXGBkaHwExSxFSJSFyMzQsH/2gAMAwEAAhEDEQA/ALf0mnQaTTY9NpsVuLEjoCGmmxgJ\nH/nPPUnnrb0v9wd16BaVTXRm4VVrtaQ2lxVPpkbvFNpUCUlxxRS22DjlxKzjnjS9lbybpTlk0nbW\njU5vng1OuhayPVLScA+PU6r1cXQou1SaXizDkkWC0arQrdDtBAE/xe25Hp38n/8ArWpL3f38iNl1\n62tvVJHUJkvpJHoVOAahWZ4STsqi9RxItFo1VdrtL3/T3sVfbGmTGuL4qfXU5Ax5EKzz+Wu3TO1n\naqnUNV2y7tpXEQFOCMh9tI8yUqB/IatQrU59WSYUk+Z0N+t8ava9xTrSs2lRJdQgw0yajUJSyWof\nGOJLaWxzcc4AVYJAGRnS5k7rbsd0ZcO84z7gSHGY5pEdEd3xCVcisBXQkKHXljSyjX3Q6jJrNanV\nWP8AaNVqcmY808eBWFuKDacHoA2EDHgNaVGra0xPsxh5pRikhtSSF8bJ5oIPp8J+Q89cvEYjEOb4\nNEn6mkpNMbdp9oPcOqtOSk1GhKdQ6EyYL9JUkxlA+8j3XgrHkTnU9pW+V1caUyKXa1RJ+63NfgrP\nPwDiHE9OXxddVLuGNUUVP+MFFd7uppTwvIAwmQjyI8T/AOdQNbdAuldQb4A+4zJSP2jDhyR54z1H\n+HjrE6mJtx05adltjDbeqZcd3fOVFpzr0zba5FyEpyhMF+NKaWf3krCgPXgz6eGl3Wu0peaFuFij\nWrQuFXutVo1BKlDpjj7lCAc+ulBAuapQyC06tOPFCyn9By1JaXuVOZAbmNGUyeqVEZ/P/uNQSx+M\nitYp+Gn5uRupU5E1pXaS3AWOKRRbFmozkKh1KQlJHj73CsZ1K6V2jp7ic1CwkJOD/mVdZdz5YC0I\n/wAdKKov7X3IrvanRjBln/3DDJZdB8+8aIJ+uuLUrPp3cl22dwHeXP2eoNIe5eisJOfnrMM1m9JL\nhfetPVEbry56eRZyPv8A22tJL9uXMyQOgjsug/VDp10KZvztrJdQzUKxJobqyAkVaC7GRk+HeKHd\n/wB7VOzTLvj/AM3UqHJHmW1oI+gVr2ZqFwx1FiS7S1Ep5tJW6Sofu4OR9NWo46p3PwuZjXm3pZn6\nDwZkSdEalwpLMmO6kKbdaWFoWk+KVDkR8te+qBWje9QtqS43Z9xN27VivvDTlFSIUtYPwrYdSAOL\npxI4Vc+urmbP35A3DsuPXojRiyUqVHnwlqyuJJRyW0rzxyIPiCDq/RrKqtrFmE+LS1mTHRo0amNw\n0aNGgNSrU6DVqbIptSityokhBQ604MhQ/wDOeeoPPRrb0aATW8u31eNZevSym0z5zqECqUd5zgTN\nCE8KXGVnk2+EgJwfdWAAcEAlc2veVDrExympecp9XYVwSKXPbMeWyrxBbV1+mdWr1DtxtsrI3Ajp\nbum34s15sYZlDLchr911OFD5Zx6a5GYZNQxr4npLtX/TVwTdxQ1/2w0iUmnEIlhs93kc8/Xx8vXS\nTmPvvuqXJdddczzLiiTn66dlY2Iv23uJW3u4ft0RPwUy5Wy8lPol9A4gPoPnpc3fZm8CXlPVLaky\nHsc5VFqTbqHfm2TxfXrrkYXJcRhJNWUl2rf3IatOUtiGr6YxrwWojoSNc+gVWpV67XLSp1sVaTXG\n1OIXEY7takqR8YJ4gkY8Tn012KvR7opSCur2ZdVPQBkrdpDqkAYyfeQFJ5fPV54Wsv8AUhVOS5HB\nrjfeQHimM0+4E+6HGg54jJweuBk48ca4EO36bKYLkiCll0K9xxkKZJGB4cuhyM8IzjONddyvUgOK\nacqDTDiTgofBaUPmFgEa+0zIjw4mpcdwYzlLqT/11NF1KceGzRLG6Vjiro9Si/6NrslIHRuUA6n5\nZ6jXHq4npc7+p01SHUnImQVEjPmU9QfXl9empkTkHHP5a81HB8RraFdp3aJE7HBo1wpcZxKcQ+2n\nrIbHw/8AET1H7wGPlqRtOJWhK0KCkqGUqScgj564dRocGW736EqiyRzD7B4VZ9fA65yDXKAsrUyJ\n8POVlkYPzKfA+o5eetpQhU6mj7BZMmratbLZ1HqXXWJ6OOIw4/5pbWgrHzSSCNdFupBJIVAqKQPH\n2YkfoTqvKlJaNEUos6kl5TEJ99CAtbbS1pGOpCSQP01LIjbkix7npdOYapq6Spt5uaHO7kvLDKXG\n5a3M80KWTw/dCRjz1Bm6zHBA9lqX/wAB3/tr2+3ZyqWqkxXqkYio64qGn6U64W2FkFbKFp4Vhs4H\nuZI8sar1MPOaS21T9PnuXcuxFPDyl9Rbr587hi3XcBRYTNVuigRqnFS0hVQiSy0lSAU4UUZyCrix\nwgEE55HWOzTXJdh7zKpjiJUSgXKmMw5ElPd45BkutqXGStXirALfPnhxsHmNQOoVOrVf2NFQRNCI\ni+NhESgOpKVAYCv2pUnI8CQceGDrfcfV9mOxKfQbhD6liQmS60nvPaEqCkOqUtYKlcSUnPp5azgI\nPBeb13slfv0+esuY5lSrNKGu2ui8T9AtZ1VSg717lVClMSGq5aAfOQ63Mor7akLBwpBKZBGQQRy1\n3o+9O4zS1F6lWRUUZGAzOkRlY8fiSsa7P6hh0+Fy/JR+4p3tcsbo0h42/ddQtInbbPKRk8SoFcYe\n5eBAcDZ566sTtB217gq1rXhSuQK3F01MhtPXxYWs+HlqWOJoz6s16myqwezQ49GozZV/WfeaHDbN\nwQqi40MusoXwvNfvtKwtP1A0anJLkm0aNGgDVY+2bu7UaR7NtdZDjy7lrHAiUuKcusNOHCGkY5hx\nzPzCef3gRYO97gg2paNWuSpHESmxHJLgB5qCE54R6k4A9Tqp/YptSbfm4Vwb2XWgyJHtbiIPGMj2\nhYy4tOfBtCkoT5cR8tAOXsw7KU7am2RIloak3RPaH2jKHMNDr3DfkgHqfvEZ6YAcSylCConCRzPP\nGs9BpVXozUtzbllWVBlvQbSpqw3cUuOspcqDpAV7A0sfCkJILqhz94IHPiwBqVS5ndzqm/Q7FotJ\nqNIjuFmdc1UiJkQ0KB95uK2f85cHirIbSQMlXQ9KRsHtPOhNs1Sy6VMfA/aygwGHXVZyVEs8AGT4\nAAeGNMSj06DSKZHptMhsQ4cZsNssMICG20jolIHIDXnXKxSqFTHqnWalEp0JkZdkSnktNo+alEDQ\nCPq/ZJ2mlhX2eiv0cnp7JU1KAOPJwL+eohVuxwyEqNC3NrUXHNKZkZLwzjxKVJ8fTTWe3bqtwZa2\nxsSr3OgnAqkv/wBOpv7yXXRxugf0EnPgdai7U31uM8dc3IotqMKOVRbdpffrx5d++c59QNYaT3Ah\nqr2UN2oYUqj3pQamlOcJkJcZUr80KGfrqJ1bZnfyhKJfspirNJz+0gyW1k/IBQV/d1Zlzs9iesOV\n3dzc2pKxlSPtkNNlWOoSE8tQ23LNnWdvQbJk7hX7SE1FK5duThVUyY05CUjvI7zT6FJ75BBORyUn\nHQ4zo6MJboxZFWrjpE6nvcd12NcdvyUn/OTDcaIPnxFIz+utWHX3owBiV6BU2vBuZlh0f1uh+uv1\nAoUepR6aiNVpzVQkIGFSERwz3g9UAkA+eOXoNcmu7f2NXUqFZs+gzyrqp+ntKV0x8XDn9daSw8Gr\nGHBM/PGLeVPQAKhGlQ1Y+IJDqD8lIzrfiXjQX+bD8h3HP3I6idWn3N7O+1aqYqVSNtajKnrUUoao\nVQTFWMgnJ75wNBP0PhgarjefZtudpZXbu3F6MNH4RLqtOkqBx0w0oHGfHUMsBSZHKhFmzFvJh1tK\nWaLX5CgMZTBwFH5qI/XW43WqjI/mLbltg/elvttgfRJUf00oJ22l527ISbitm6KVE4MuyHaS6ptK\ns+aVAEYyc5+mpjt1tlTLyktQba3Ztj25ZwIdQM2G8s/hAPJR9Ek6rvK4cvnuiB4KPIllJhyY7kx1\n4o4pUgv920lXC2SACATzOSM+HMnXRShR+4r+zrrsdlDcQHKrhthQ9ZtRP+BGttjsq30k5crdpq9F\nO1FY/Vwajnls5O7fz1NJYSTe5wk8aOY4k4+mvr7WbiNqddnMttoGVKW6kBI8yT01Fdx9sJW3N6UW\nlX5Pg0y3qrnFdpVNVJDTgOFIWl5eQAMEkZODkA4IFqttez3tna7TM4wVXLLIC25dVUl9AyMgttAB\npI8QQkn11hZSpdZ+wWBvuxT7VWxWtwLrpFwUiKqBTaXMbkG4lJLa3QhWVMxvF0LGUqUf2YBPxHA0\natu2hLaAhCQlKRgADAA8tGupQoxoQ4I7F2nTVOPCj60a85LzUaO5IfcQ002krWtaglKUgZJJPIAD\nx1Va/O2HAjXKui2FaL9yIQ5wJlrfU2l8jr3baUKUU+ROM+Wpjckvb4uNVN2aZt2KpSptwVFqKlpH\nxLbQe8UB5+8Gx/W01NkLPRYe1VvWvwJS/Dhp9qKR8T6/fdPr76lfQDSb2ft64t2twmN0N1LeqtOV\nSfdoNJfi9zDjniyHPfV3ri8+9koSnIHM4AFlxy0B5yu99nc7gJLvCeDiPLixyz6Z1o2zR41Cosem\nxskNgqccVzU44olTjij4qUsqUT5nXT1g8wQDoCL3VcFUTOFAtWCzOrK0BTrsgqEWAg9HHinmSfut\nJwpeOqU5UI67ZNo0h9u6tx64zXakyeJM+vOttxYp8mGCQ0yB4EAr81E89fO/G5NF2esGRWjGZeqU\ntakQImcGVII5rWepSkYKldcADOSNUk7SNoX3GoVtbhXncX8YJNx98445HdDsWF8Km2W1A8IykqOE\n+77pAzgkgXQqvaK2Wpji2377pzqkcsRmnXx9ChBB+h16UrtEbMVJxCI9/UxtSxke0odYA+ZWgAfn\nr8x6UuImpR1VFDjkNLqC+hs4UpviHEEnzxnGrSVfajaaRc20UC14FTfXdD6pVQgSZpcfNPUOMLc7\ntX7MpSTjhI+E5yQdAXYolbo9chibRarBqUY9HokhDyPzSSNQjfq3VVihUOrwwpNToNfgVCKtA97H\nfobdT6gtrVy8cDSoq/ZTboE37c2lvus2tVmxxIbfdLjSv6JUgBQT+8Fj01Otmb9u9y4ndtt2KXHi\nXbHiiXFlRilUapxwrBdTjklQI5jl8knloBxjWdGsaA16pMj06myahLWG48Vlbzqz91CQVKP5A6h2\nx02pV3b2FdlZdcXNuHNT7oryiOy4cstIHQJS1wD1VxE8ydSe6qait2zVKMp3uxPhPRisdUhxBRn9\ndV43t35h7JW5RrAtqNFq9yU+nR47weUSxDQhpKU8fDgqWoDIQCMDmeoBAszgeHLS63M2U253AZdN\nct2M1OWPdqEJIYkoPnxpHvfJQUPTVabK7Q+/V9x31W5EsgyES48REVSC2+449xcPAlbvMDhJJPQD\nXVsPd/tQXG3V3qNaNvVg0iYYU+O4wll1l0fEnh75JOMEcs6AZm3sy+Npb0ptgXvVXbitSrr9mt6v\nOg98w/glMSQcnmoDCDz54AOMhL60jbXv6DunZku0rzozlr3j37kdNOkpWnhmMJQ+0tlagPeGUOBO\neLAJGUjOneypRZQXOELKQVYPLPjoCtf8IlUY8bZyl01aQp6ZWm1IHiEttOFRH5gfXTk2LTPRsxZi\namVGWKHE7zi6/wA0nGfXGNVv/hAJcOrVGy2zLC6TTqm9Dqq2xkR3XEsL4Sfxd0CcdRkat5T/AGX2\nFgQi0Y3dp7ktkFPBj3cY8MYxoDY0aNGgKzduG9asYNF2ltXvHazdLqUyG2z7xYK+FLfoHF9T+FCs\n8jruW1A297NlnUmkrirqt3VfhZSiAwHJ9UfPUIBxwtAnAyQkcuqidRHbSKm+e3He1yTMOx7UZ9jh\np6ht0AMDGfk+fmddLs2NDcjey+93KoPaEQJhotCSvpHZSDkpHgeAo+ri/PQDrpdwVmDQZFw32zR7\nbhNNd6tkSy8qOn/eO4Sgq54wkEZOAVeOircSc8wqXTNt72qMLBKH0xo7BcHmlp95DvyygE+WtKoo\nF2b2oo8pIdpFpwGaiphXNDs+QtYZUodD3TbS1JB6KcCuqQdMgAAY0As6dvpt07VBR6zVJNr1XxhX\nBDcgr8vicHAforUjrFqWndiW6n3QU+rm1UabMXHe+jzCgoj6ka3b0tC2bzpK6VdFFh1WGro3IbyU\nHzQr4kH1SQdVzubsnVClS3Z21G4lWtwrUVCG/IcCEn0daIUB+8lR9dAOm3to7PpdZVWZqancNS7h\nUZqRXqg5UFMsqGFIQHSQkEEg8skEjOCdQ64+zLYNWp8ilRaxddGoz73fmkQaqfYUu5zxhlaVAHP5\neGlSztP2tYq0ts7nJWhvklSq46oEDofebJP10xbO2i3hlKbdv3fKtrZHxRaGe6KvP9sUg/3M+ugI\n8rsUbe8Xu3RdIHqtg/8A562qd2OrRpkoTKXfN4wZSQQh6O+y2tIIwRlKAcfXTgM/b7biDmp3NFgF\nCOFT1WrKnn1+fN5alk58B+WljeHa423psj2G2Y1Wuyco8LaIUcttqV5cS8KP9VJ0B1KXs5uXb6Um\n3t/LiPB0aq0Buc2ryHvryB8tde09vb4f3ah35ftfos1yk0xyn05mkxnGEuFxRK3XQsnCiOXCCRnB\n5Y58zb+o7y7jymajc0Rnby3CoON09gcdTmoyPdUtf8yjpkhKV+Axni07kjAA0BnXw+2HWVtKKkhS\nSnKVEEZHgR0Prr70aAr5urZu5FiW7XrxszeCrCNEiOyXqfcPDLbCQkk926oZSofdBSeeATqvEjs5\n72V2AoTLeo0h+dK9tdq8iptqlLK08+NfGcoOc44Sc89Ww7YEWdM7Od3NU8q7xMdp1wAZJaQ82pwf\n2Qfy1P7SrdAqlNZaodZp1SRHaQ2ow5SHgjAxz4ScdNAVM2n7I9+W9XYdwyL/AKfQKjG4i0umxTKc\nb4klJAU5wpB4VEZwcZ027H7PKrViPRoG619x0SZC5MoQ5DLAeeVjK1EtqJJwMkk6cldqP2VTHagY\nkuWhkBS2orRddKcjJSgc1YHPAySAcAnlqHy96NqYlPXNlX9QWUoyFMrlAPpI6gs/zgPpw50Brxtn\nrZFGqcGpTq3WZVRkMynapPnFU1t5gYZcacQlPdqbyeEpA6nOQTrRuKwLldaRDd3tuuBTV5QU93Cb\nkLz4B8NJVn6E6Vl9doq9LvW5RdjbHrNS4/dNbfp6ihPq2gjhH7zh/q6X8Hsu7x3/ADjW9xLqjwZL\np94zJKpshOfAJSeBI9Ar6aAs3cmzdkVDZ93b19pxilICpAmLd4pCJGSoylLV8ThJJUTyIJHToitr\nb+u3YS4INj7lSRU7GnAfYlwsEusNtnmkoXzy1gjKOZR1TlPV+7Ibc1Xbegqosy+KpcsRIHs7cxlK\nRGx1DZyVBJ5e6VEDHLGTpa3ZZlJjbkSNnak3xWXfUKTPpDAGTSKkz77pY/Agg8YSOQJIAwSCBYxl\n1t5lDzS0rbWApKknIUD0II6jRpH9jeuVZ2w6rY9fdLtUsyqO0haz1U0knu/oMKSPRI0aAjfZjZcg\n9pHe6HIaw6uptvg/0FvPKH5haTrd7D7n2ZQr4suYhLVTotzP+0I+8UrASlR9MtKwddStRBYva0pl\nyrSlFJvmmGlPOEYSmezwqbBPmtCAkeZzrw3btq49v9zU71WLTHasxIYEe6qNHH7SSwkDEhseK0gD\nPj7oPQq0BL6KpNE7R1wQ3U8KLloUSdGWfvuRFrZdQPUJdZVjyOdM7S3qDdP3Ptu2L6sarxxOp0oT\nqZLcSeBSVDgkRnQPeSFoKkKHVKkpPPHNjpzw88/XQGdGjRoDB0ttwdpNqqyJVauO2mUuKyt5+I4+\nw44o+JDCgXFEnpgknz0ytecl5mOwt991tpptJWta1BKUgDJJJ6ADx0BTqrWVY9HqBeo3ZZvOvQkr\nUfap019DiwOqgyVKUR5BQB9AdSuzdzYNtOGLbXZhu2jyyAEiJSEtlQP4nOAEfXOn9YteN0URNebh\nqjwJbil08ryFvRuQQ8Qfh48FQH4SnPMkDvn10Aq6DWKzSIFV3P3VEO12G4gZiUwSO+9hj8XErvFj\nk4+4vgHCkdEISOedcDa299z92Zr9x0eNT7SsYrKILkyIZM6dwqwVpHGEITyIzggcwOLBIqv2q931\nbj7j/ZTMp4WhSJXcsNNq5SClXC5II8SRxBPknHQk6vSze23dvbew68zcFHh2wzFQmI8h5Pd92lIC\nUISOZUBgcAHFnljOgJkBgY1nVfaBuJu9upWBP23oVNtuzG1nu6vX463HZwHi20lQPCfT+0Dy076F\nImiExFrcinqq6WQuQmIVJQeZHGlCiVJSSPHODyydAeV1WxQLqhIg3FSotUhpVxiPJRxtFXgSnoSP\nDOcaV9ydnKxVuirWR7ZY1wsZVFqNIeWkJV1AW0TwqR5gYyOWdOfiT5jVd9zt86nXb6i7XbMGNULg\nkOluZV1J72NT0j41J+6spGSVHKRyA4lHkBt7Nbx1pq/pe0m67caJdsVzu4c9pPAxU04ykgdErUn3\nhjAVzGAoYLvlUiky5bc2TTIT8lHwPOR0KWn5KIyNVb337PF5VSjyb4F/VK57qpMVCoiTT2Yq3Etr\n4yElvmVjKlI8cjGemvfavtK1+7KdTafAptGn3Q0z3MukTZvsK57g6PRXiCgqIzxMqCSDzSSM6AtU\nlCUpCEpASBgADAH01nSUoO/rUW7Ilrbl2bVLBqM4fyN6a8h2G8oHHCHkgAHJAz0yeZGRl1gg9NAB\nOBk9NVy3MumHK7QKbhWlS6DtbR5Uuovg+6udJb4G46T0KyOHHrkasaeY640i9/KXaO3+3zFcmOJa\no9KnKqQpIGVVqqqOWDIcJKnAF5WoHOeEEnCeEgafZXkrqW5G7tYDZZak1qOlTQA4UPpbX3oz4kKO\nD+ujXW7GVuVCi7MsVisgmqXLMerMlSx7570gIJ+aUhX9fRoBhbpWTTb/ALNlW7UXXoxcUl6LLZOH\nYkhB4m3kH8ST8sjI8dQW3d1ZNoFu2N5mfsOpMkNMV7u1fZlUSOQcS6BhpwgZUheMHp1xpx6832Wn\n2lNPNpcbUMKQsApI9QeR0By7TkW1Lp7ku1naS9DkPKeW7TVtqbdcV8SyW+RUeWT1OuxpCx34+0Pa\nDg27CYRDs+/QtxhhCAlqJVUYCuD8KXE8A4enEoYwBp9aANGjRoA0n+0TUXqxOtbaeA6tt675pRUV\ntnCkU1kd5JGR0KwAjPkVacGkVDKqv225qnlpU3QrOQhlHP3VvPAk/PCyPljQDwjstR47bDLaW2m0\nhCEJGAlIGAAPIDlqDxLrcuu274XRQOGlSJdLiupVzdeaYHGrywHFFI/dz467e5lcNtbeXFcCThdO\npkiSjln3kNqKf1xpXdh9l5XZ1pT8s94qZMmPKKuZWC+oEnzyQdAUp7NNp2/fO71KtO5UOmBUWZCC\npp7unEOJZUtCknzynoQQfLVzrL7J+09u1FmfJjVKvuMq40N1OQlbOc8iW0JSFfJWQfLVP7RSrbHt\nTQY0jLTdHub2ZxROP2JdLZV8i2vP11+nSemPLloD5ZabZaS002ltCEhKUpGAkDoAPAagm7u3Avdi\nJOpddm21clN4jTqvCOHGgrHE2sZHG0rAynPUA+YM+0aAqVcu0XahudK6HXN1aW7RXBwurZeWyXUe\nIUhtpKlcuqScHTl2A2XtzaOiONQFGoViUAJtTdbCVuAdEITz4GwefDk5PMk8sNDRoDB5jGqX9r7s\n8y40+XuLt/CW404ov1WnR0++0vqX2kjmUk81JHMHKhyzi6OsaAoXsjuTB3VpjWzW77i6kxOPBQ6w\ntQ9qjSAMISVn4ieiVHJJPCriBGHZtLc907V3tD2f3Jlqn02XlFqXAoEJkpHIRnCc4WMgAE5BwnJB\nSddLfTs8WvdUaVclqREUC8I/8qiSYeG25D6DxJDiPhyVD4xg5IJz00VBuH2iezmXGkoiXIwMoHwr\np9VY6pz1SCrln8KwfDQD2zyzg6p1ueqTv52qYm3rDizadqKUuolB5LKCnvzkfeKuFkeXvHz1Zij1\n6oQtn4lzXAwpmoRqCmdUGnBgoeQxxuA+WFA6Rf8AB70RTlmXLfM7LlQrlUU2XFDmUNjiUQfVxxf9\nkaAs9HZbjsIYZbQ222kJQhAwlKQMAAeAA0a9NGgDRo0aAVXaksWVfG1UtNISsV6juJqlJW38Yfa5\n8KfVSeID14ddzYu/Yu5G2VJudkpTJdb7qcyP9TJRgOJ9OfvD0UNTg9NVspqhsf2lHqW4QxZO4The\niqPJuHUQeaM9AFFWPktH4dAWU0awCCMjWdAGktEirpPbIlyX0YZrtngx3OgU4xIQFo9SElJ+R06d\nQrcmmBNVtm72UZfoNQJePPPskhJZfHyHEhw/8LQHL7UDqmuz9eqkEgmkup5eRwD+h1zex7HbjdnC\n0EN5wuO86c+an3Cdd7tDRVTdi72jtt96s0SUoJHmlsq/TGfprh9kF1t7s42eppYUExXEHHgoPuAj\n89AVY7f1oOULd6LdMZsoj1+IlZWOX8pZwhX14e6P1Orr7S3Ii79s7cuRK+NU+nMuu+jnCA4PosKH\n00vu2XYpvXZSpORWe8qNEP2nFwOZCAe9T9Wyo480jUW/g+Ll+1dn5lvuuFTtEqK0IGejLw7xP97v\nNAWT0aNGgDRo0aANGjWNAZ1WXclyXsNvczuJDQ4bGu6QmPcMdAymLK54kAeBPNXrhweKdWa1CN6B\nYcuyZFC3EnRYVGq6hF76Qvu0pc+JBDhGEKBGQTyyPHpoDX39mFOwl5zIb6VBdAlFDiCFJUlTR5g+\nIIOop2I4zbHZstpaAQXly3F5PU+0uD/ADW1e9uGhdk6vW3Eq7lbbg2xIaYmLAy80ltRQeRIOEYGQ\neeM6+exgoK7NVpYGMIkj/wCy7oBxaNGjQBo0awo4GdAcu7K9TLYtyfX6zIEeBBYU8+5jJCR4AeKi\ncAAcySBpZ3LQ4G/2zMql1eAaHWGpDiO7WvvXKZOaJCckYyOEpJx1S5y8DrQuKp/5U9w2qXCV3tl2\ntMD018HLdSqSPgZT+Jpk+8o9CvhHhrers5/b+53r4ZQ47b81CGrjZbTxKY4BhuclPU8A9xwDmUBK\nvua+Vr9LMHRziGWSerWr5KTtaPmr+bROqEnT4yK7G70VCj1cbVbyZo91wcMRJ0k8LNSR0QeM8uIg\ncl9F+isg2LBzqEblbdWRutbbUW4IDE5lSOOHOjrAeaChkLacGeR5HHNJ5ZB0oGG959hU92hMjcyw\nWegT/pOntjyHMqSB+8OX+rGvqiAsvr5cQhxtTbiQpCgQoEZBB6jUL2u3Ssncine02tWG5DyE5fhu\n/s5LH77Z54zy4hlPrqa6A59wUtmrW7PozoAZmRHIqgRkcK0FB/x0jewhVHFbQS7WmEpnW5V5MJ5s\n9UhSuMfTiKx9DqwWq7mG7s92mZNaeHdWbuEpLTr+cNw6mOaQvwAWSvBP+0P4dAWElMtSIzkd5AW0\n6goWk9CkjBH5HVLOxd31i9ou89upilIC23mkBR+NcZ3KD65bWo/LV1wcjVR+0hS1bb9p2yN24ie7\np9TmNxakpPIBwDulk/vMqz82zoC3OjWE9PPWdAGvKVIZixnZEh1tpppBW4tasJSkDJJPgANKvfPe\niBt89DoFHpj9x3hU+UCjxclXPotzhBITnoAMqwegBIW6tsu0FunGV/lHv6PalGlDDtFpTYUotnqh\nfCQDn+ktfqNAbFP3O3q3bqc+TtBS6DSLUiSlRmqtV8qXJUnqpKeeBgg4CTjIBOeWt/8Ain2szHKv\n8qloB/8A2f2ejh/tez/9NdXda+rX7Nu1VJoFuwESJxQpilwnXPjI5uSHlDnjiVk4wVKVgYHMJSzN\nxu1bULmp9yfxerVTo0hxKjC+yENRnWCeYSSApPLOF8XlkkaAZMqudrKywqTUrftm+ITf879njgfI\nHilKeBWT6IV8tTjaPdaz97KPVLcn0ZUSpsNFFVodSbC/cJwSMjC0hWAcgKScZA5HTdQeNAUUkZGc\nHqNQiVtza0bdJO6Se9g1ZqA5FlKbWEsyGyB77oxzUkJxnPQDOcDQC0l27J27VcO2cKVKfs+57fqT\ntDadUXXKdJbZPfRkk8y2pC+NOehSoeZPW7D0oSezdbqAnHcOy2j6/wAoWrP97Xtsiuq7nwYm51zz\n1KjrenNUSmsshpmMwpZZ70q+NxakIIyo4AUrA565HYVzH2jqlFUslVJuKZEKTzCccCsA+PMk/XQD\n/wBGjRoD5cWltBWsgJAySfDSNvC8qvufUpFo7eTVxLbbWpit3Qz0UOio0I/eWRyU6PdSDyycZ9q8\nisboVqsUyrzjTbOpdSep7tNhOKD1UW0RxGQ7yKWef80jrz4lY5a4U68ZlSdNj7MU6C4YWI0mr90B\nTKSkfdRgYdcHghOQD1zz18F0g6Vzp1J4LL1/cj1pvSFPxb3fd+XoWqVBO0p7fk7FcrdKsGkU2w7J\nprEquuMdzSqO2o8LaPF99XVDSSSpSzzUeQyTr1tKt1ijVVqzL9nMTJ8lJVTKqlgMtVIYytoo+FDy\nOfufeRhQz7wEUqjtG2fpn2dRESbr3GuRWGy+rvJc97GO9dP+rYR1xkAAY81Dr0qf9tRBttuzGjfb\njzfExIQO7j1QDmHYyxjgeQeqRhSSniTyPLzGeCg8O5qPHCTbc3/llbepBPXgi9LX/ddtu6vC6pa/\nLeBvwJ0zZ6oLUUOytupDhWtCEla7eWo5KkpHNUQk5KRzazke7p1wJcSoQmZsKQ1JjPoS4y80sKQ4\nkjIUlQ5EEeI0kU1+rWKRT74ecqVvk8Ea4+74i0k8g3OSkYSfDvgOBX3gk9dSeZu0VGqF32VKjTLP\naaM2Xbzzh7lAOCpyE6nPdZznuyC2cnHDr0Doz0tlBQwmYSvxaQqK7UuxPsl469tt3VrUN5Q9CU7n\n7E2ld9RFwUpcm1bqaUXGa1SFdy7x+biRgOep5K/pahsPdLcDaKa3R966WqqUNSw3Gu6lMcTZ8hIb\nA91XqAD5BfXVhozoeYbdSCAtIUAfUZ18TokWfDdhzYzMmM8godadQFocSeoUk8iPQ69LKRqW5XaP\ncdIYq9CqUWpQJCeJqRHcC0K9MjofMHmPHXpXaRTK7SJNIrMCNUIElHA9HkNhaHB5EH8/Q6RNx7BV\nm1avIubYu6XrUnOK436O+ouU6UfLhOeD6hQHhw60ovaMuCyZDdK3q27qtvv54RVKcjv4bx/EOfT0\nSpR9BoCwtGp0ak0uPTYZe9njNhtrvXlOrCR0BUolRwOWSSeWlL20KEmt9nyvKSjikU5TM+OQMlKk\nOJCiP6il6lNlbx7ZXhwIoN50l99eOGO693Dx9O7c4VH6A6m0yPGnRHIsplt9h1PCttxPElY8iD1G\ngNN6YaPbC59SKnDChF6QUcyrgb4lY9eR1yr3vGDbO29RvVxCn40SAZbbY6ukpBbQPVSlJT9ddysw\nGqnR5tNeUQ1LjuMLI6gLSUk/rpTbE1KBd22qtubvYQuu2upul1eA6ohSvZ1J7h/wJQsIQoK6Eg9f\nEDZ2E2ydt/2q+7xKahf1f/b1GUsZ9kSrBEZr8KUjCTjrw46AaZ1cnOU2lPzWafMqLjScpjREpLrp\nzjCQpSU5+ZA1iuVik0Kmu1KtVKHTYTQy5IlPJabT81KIGtxpaHWkuNqCkLSFJI6EHmNAIK29pa5f\nW7bu6W69OjRkxghqh273yZCYraclK31DKVKySrhGRxHJ6Aaf4SAPP56zo0Aag1+bY0W8n3PtOsXN\nHiSE8EyDCrDzEaUnkClbYOMEDB4eHPjqc6NAaVGplPolIi0qlRGocGGylmOw0nCG0JGAkDSV7MSB\nTNw94bdBUBGukzUpKwfdkJUoEAdOSR+nlp7HSN21bVB7XO6cdAb7qdTaZLOE4IIbCf8Aqo6AeejR\no0BXCl2pU7trN4U+pVtyHawuif3sGAVNyJyuJPEl57OUteHAjBUCcq8NEq81POfxC2To0CU7CHcS\nKilsJpVK88qTydc/oJzz655jXlb9oVG7513wqncMmJbv8baoH6dASWXph774XX88Xd4x7iAnI6qO\ntyobgUC3HEWHtbbBuatxk8CKZR0BMaGemX3h7iOfXmTnqRrxLNqdTF5vWoUYfWmpNqCVqcX/ACm9\nOJ+3Jt7HSptRppvTv5+R1bNsu3NuYVQuq4KwJ9aeR3lVuCqLCVqH4Uk8m2+QAQPIDny16UatWBvR\naUtmKpFSgtP8LjbiS0/HcHwOp+82SPeQsYP6jXOt3ZO4bzq0e4t7q01VAyvvYttQCU06OfDvD1dU\nPH8ipQ5amG5u1LVZlsXLZk5Fr3bBZDUaYw3hiQ0kYTHkNjktrkAOWU8sdMa7H9P8XXpPE18S/utH\nFrSMbcvDwslyRH91FOyX7SHrrd1bftrh3fCnXXbYBS3XIcfvZTDf4ZjCeawByLrYOQPeTk6g+49B\ns2Zsxdlxbb3XKjUg091yRTKZKSuA4o9QphaSWCfEI4D6anFC3Ybp1XRa26FLVZlx5w2qQr+QTfDj\nYf8AhwfJRyM4yTrjdpGwLSk7bXNd0amoiVdumuOe2QHFMGQORw6EEJdSf6QPz18xhVWy/MqdHG05\nUakpR1ik4T1W8Xp5xen8bkztODcXde5Y+knNMin/AHKP+Ua2talHGKVEH+4b/wCUa29e9rY5Ya8J\ncSLMiuRZcdmRHcHCtp1AUhQ8iDyOvfRrIEtuB2Y9pbt7x5FBNBmKyRIpKu5GfVsgtn+yNKiZsnv9\ntesytq9wX63Ab5ppz7gbVjy7p0qaV8wUn01cDWDzGDoCoVvdq667TqaaHvDYEyFITyMiKyphwjOM\n9y4eFfzSoDyGmCh/Y3fCsRa5Rbpdpt0NNhtEmmzlU2phP4CDgrA6dFY8Dp03Fb9DuOmqptfpEGqQ\n1dWZbCXUfMBQ5H1Gq/7hdj6wa0pcq1J8615hPElCCZMYHr8Czxp5+S+XloBj29sjY9NqzFYqYq10\n1SMoKYl3DUHJy2iDkFKV+4CMDB4cjA0zBqnUO2u1jtR+yoVSavSjsn3WFPCUOEdBwOlLqfkhRGu5\nTu1pUKC6mHudtfXqE+DhbsdCgCfPu3gkgfJR0BarRpHUPtV7LVIJD1xyqatWPcmU90Y5eJQFJ/XU\njib/AOzcoZa3Boyf+Ktbf/MkaAZ2jSqqPaJ2XgBRev6mucIyRHQ68T8uBBydQC7O2NtvTkqaoNNr\nldkfc4WRHaV/WWeL+4dAWTUQPn4aRWzcyPdHaT3NuukOJkUiNHhUhEpCgpDz7acucBHIhJGM+oPj\nqAtVLtCb9p9iagDbizJI4X5JSsSH2j1SkqwteR+EISfEnpqx+2lk0Db2z4dr25FLMKMCSpRy484f\nicWfFRx/gBgADQEl0aNGgFHE2qodzGdVU3rcE23a3MdqP2bCkojR3C7jjStxtIeUnI+ErGOYI0xL\nVtm37VpLdKtyjwqVCb5hmK0EJJ8zjmo+pydGjUdOjTptuEUr6uy3fazLbZ2NGjRqQwcq57doVz0t\nyl3DSYdUhOfExKaDiM+YB6H1GDpYSuzftyth+FDVcdPpj6Slymxa5ITFUCfFsk8vTp6aNGtZQjLd\nXA4Wm0tNJbQOFKUhIHkBr70aNbANGjRoA0aNGgDRo0aAxr4fYafbU282l1ChgpWOIH6HRo0BFqxt\nnt3WMmp2NbctROStymNFROMdeHPTUWmdnTZWXw99YNOTw9O5eea/PgWM6NGgNdHZo2PQsLFhxyQc\njM6SR+Rc56mVqba2Daq0u29Z9EpzyejzMNHe/wBsgq/XRo0BKwMazo0aA1KtUYNJpsipVKU3FiR0\nFbrrhwEj/wA5Y6k8tGjRoD//2Q==\n"
    })
    Post.post({
        "judul": "Cincin Emas",
        "deskripsi": "Mimpi setiap orang!",
        "id_category": "2",
        "url_video": None,
        "user_alamat": None,
        "user_kota": None,
        "created_by": 1
    })
    Comment.post({
        "id_post": 1,
        "pesan": "Saya tertarik!",
        "created_by": 1
    })
    CommentReply.post({
        "id_comment": 1,
        "pesan": "Ok, PM untuk detailnya!",
        "created_by": 2
    })
    Chat.post({
        "id_sender": 1,
        "id_receiver": 2,
        "message": "Halo gan, berapa harganya?",
        "created_by": 1
    })
    Chat.post({
        "id_sender": 2,
        "id_receiver": 1,
        "message": "2.7 juta untuk versi 6GB RAM/32GB ROM",
        "created_by": 2
    })
    Chat.post({
        "id_sender": 2,
        "id_receiver": 1,
        "message": "3.2 juta untuk versi 6GB RAM/64GB ROM",
        "created_by": 2
    })
    Chat.post({
        "id_sender": 1,
        "id_receiver": 2,
        "message": "Deal gan, cek halaman Deal nya",
        "created_by": 1
    })
    Deal.post({
        "id_post": 2,
        "id_poster": 2,
        "id_penawar": 1,
        "judul": "Wish - Xiaomi Redmi note 8",
        "keterangan": "6GB RAM/32GB ROM",
        "accepted_at": None,
        "harga": 2700000,
        "created_by": 1
    })
    ReportPost.post({
        "id_post": 1,
        "id_user": 2,
        "keterangan": "Report test deal!",
        "created_by": 2
    })
    Trans.post({
        "id_deal": 1,
        "keterangan": "Konfirmasi secepatnya gan!",
        "created_by": 2,
        "alamat": "Ngagel Jaya 1",
        "kota": "surabaya"
    })
    UserFriend.post({
        "id_user": 1,
        "id_target": 2,
        "created_by": 1
    })
    UserReview.post({
        "id_user": 1,
        "id_target": 2,
        "comment": "Good santa claus!",
        "rating": 5,
        "created_by": 1
    })
    Wishlist.post({
        "id_user": 2,
        "id_post": 2,
        "created_by": 2
    })
    UserBlock.post({
        "id_user": 1,
        "id_target": 3,
        "created_by": 1
    })
    ReportUser.post({
        "id_pelapor": 1,
        "id_user": 3,
        "keterangan": "Mencurigakan!",
        "created_by": 1
    })
    print("Seed (for mock data) successfully created!")


def seed_json_file():
    json_files = []

    # r=root, d=directories, f = files
    path = Path(os.path.dirname(__file__) + "/json")
    for root, dirs, files in os.walk(path):
        for filename in files:
            json_files.append(os.path.join(root, filename))

    for json_file in json_files:
        with open(json_file, 'r') as item_json:
            products = json.load(item_json)
            for product in products:
                post = {'created_by': 1}
                for key, value in product.items():
                    print(f'{key} : {value}')
                    if key == 'kategori':
                        try:
                            post['id_category'] = Category.get({'nama': value[2]}).id
                        except Exception as ex:
                            break
                    elif key == 'nama':
                        post['judul'] = value
                    elif key == 'deskripsi':
                        post['deskripsi'] = value
                    elif key == 'gambar':
                        post['url_photo'] = value
                if len(post.keys()) == 5:
                    Post.post(post)
    print("Seed (from JSON file) successfully created!")


def seed_category():
    Category.post({
        "nama": "T-Shirt",
        "created_by": 1
    })
    Category.post({
        "nama": "Pajama",
        "created_by": 1
    })
    Category.post({
        "nama": "Sunglasses",
        "created_by": 1
    })
    Category.post({
        "nama": "Sweater",
        "created_by": 1
    })
    Category.post({
        "nama": "Jacket",
        "created_by": 1
    })
    Category.post({
        "nama": "Hat",
        "created_by": 1
    })
    Category.post({
        "nama": "Pants",
        "created_by": 1
    })
    Category.post({
        "nama": "Shoes",
        "created_by": 1
    })
    Category.post({
        "nama": "Bag",
        "created_by": 1
    })
    Category.post({
        "nama": "Processor",
        "created_by": 1
    })
    Category.post({
        "nama": "Motherboard",
        "created_by": 1
    })
    Category.post({
        "nama": "SSD",
        "created_by": 1
    })
    Category.post({
        "nama": "Hard Disk & RAM",
        "created_by": 1
    })
    Category.post({
        "nama": "VGA",
        "created_by": 1
    })
    Category.post({
        "nama": "Unit Printer",
        "created_by": 1
    })
    Category.post({
        "nama": "Cartridge",
        "created_by": 1
    })
    Category.post({
        "nama": "Aksesoris printer",
        "created_by": 1
    })
    Category.post({
        "nama": "Tinta Printer",
        "created_by": 1
    })
    Category.post({
        "nama": "Cooler Fan",
        "created_by": 1
    })
    Category.post({
        "nama": "Flashdisk",
        "created_by": 1
    })
    Category.post({
        "nama": "Keyboard",
        "created_by": 1
    })
    Category.post({
        "nama": "Mouse",
        "created_by": 1
    })
    Category.post({
        "nama": "Modem",
        "created_by": 1
    })
    Category.post({
        "nama": "Mini PC",
        "created_by": 1
    })
    Category.post({
        "nama": "Laptop",
        "created_by": 1
    })
    Category.post({
        "nama": "Desktop",
        "created_by": 1
    })
    Category.post({
        "nama": "Scanner",
        "created_by": 1
    })
    Category.post({
        "nama": "Monitor",
        "created_by": 1
    })
    Category.post({
        "nama": "Masker Wajah Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Masker Wajah Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Pembersih Wajah Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Pembersih Wajah Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Suplemen Wajah Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Suplemen Wajah Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Wajah",
        "created_by": 1
    })
    Category.post({
        "nama": "Mata",
        "created_by": 1
    })
    Category.post({
        "nama": "Bibir",
        "created_by": 1
    })
    Category.post({
        "nama": "Kuku",
        "created_by": 1
    })
    Category.post({
        "nama": "Rambut",
        "created_by": 1
    })
    Category.post({
        "nama": "Aksesoris Makeup",
        "created_by": 1
    })
    Category.post({
        "nama": "Perlengkapan Mandi",
        "created_by": 1
    })
    Category.post({
        "nama": "Perawatan Kulit Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Perawatan Kulit Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Perawatan Rambut Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Perawatan Rambut Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Peralatan Cukur",
        "created_by": 1
    })
    Category.post({
        "nama": "Pria",
        "created_by": 1
    })
    Category.post({
        "nama": "Wanita",
        "created_by": 1
    })
    Category.post({
        "nama": "Unisex",
        "created_by": 1
    })
    Category.post({
        "nama": "Import",
        "created_by": 1
    })
    Category.post({
        "nama": "Agama & Kepercayaan",
        "created_by": 1
    })
    Category.post({
        "nama": "Hukum",
        "created_by": 1
    })
    Category.post({
        "nama": "Komik",
        "created_by": 1
    })
    Category.post({
        "nama": "Console",
        "created_by": 1
    })
    Category.post({
        "nama": "Game Watch / Portable",
        "created_by": 1
    })
    Category.post({
        "nama": "Games (CD, Cassette, etc) Original",
        "created_by": 1
    })
    Category.post({
        "nama": "Joystick & Gamepad",
        "created_by": 1
    })
    Category.post({
        "nama": "Aksesoris",
        "created_by": 1
    })
    Category.post({
        "nama": "Keyboard & Piano",
        "created_by": 1
    })
    Category.post({
        "nama": "Gitar",
        "created_by": 1
    })
    Category.post({
        "nama": "Bass ",
        "created_by": 1
    })
    Category.post({
        "nama": "Biola",
        "created_by": 1
    })
    Category.post({
        "nama": "Aksesoris Gitar",
        "created_by": 1
    })
    Category.post({
        "nama": "Action Figure",
        "created_by": 1
    })
    Category.post({
        "nama": "Static Figure",
        "created_by": 1
    })
    Category.post({
        "nama": "Model Kit",
        "created_by": 1
    })
    Category.post({
        "nama": "Diecast",
        "created_by": 1
    })
    Category.post({
        "nama": "Board Games",
        "created_by": 1
    })
    Category.post({
        "nama": "Camera",
        "created_by": 1
    })
    Category.post({
        "nama": "Aksesoris Action Camera",
        "created_by": 1
    })
    Category.post({
        "nama": "Drone Unit",
        "created_by": 1
    })
    Category.post({
        "nama": "Sparepart & Aksesoris Drone",
        "created_by": 1
    })
    Category.post({
        "nama": "Diffuser",
        "created_by": 1
    })
    Category.post({
        "nama": "Screen Protector",
        "created_by": 1
    })
    Category.post({
        "nama": "Lenspen",
        "created_by": 1
    })
    Category.post({
        "nama": "Cleaning Kit",
        "created_by": 1
    })
    Category.post({
        "nama": "Blower",
        "created_by": 1
    })
    Category.post({
        "nama": "MTB",
        "created_by": 1
    })
    Category.post({
        "nama": "Roadbike",
        "created_by": 1
    })
    Category.post({
        "nama": "Fixie",
        "created_by": 1
    })
    Category.post({
        "nama": "BMX",
        "created_by": 1
    })
    Category.post({
        "nama": "City Bike",
        "created_by": 1
    })
    Category.post({
        "nama": "Handlebar & Barset",
        "created_by": 1
    })
    Category.post({
        "nama": "Shifter",
        "created_by": 1
    })
    Category.post({
        "nama": "Stem",
        "created_by": 1
    })
    Category.post({
        "nama": "Cable & Housing",
        "created_by": 1
    })
    Category.post({
        "nama": "Brake Set",
        "created_by": 1
    })
    Category.post({
        "nama": "Disc Brake",
        "created_by": 1
    })
    Category.post({
        "nama": "Rotor",
        "created_by": 1
    })
    Category.post({
        "nama": "Caliper",
        "created_by": 1
    })
    Category.post({
        "nama": "Piston",
        "created_by": 1
    })
    Category.post({
        "nama": "Gear",
        "created_by": 1
    })
    Category.post({
        "nama": "Filter",
        "created_by": 1
    })
    Category.post({
        "nama": "Speedometer",
        "created_by": 1
    })
    Category.post({
        "nama": "Koil",
        "created_by": 1
    })
    Category.post({
        "nama": "Box",
        "created_by": 1
    })
    Category.post({
        "nama": "Alarm System",
        "created_by": 1
    })
    Category.post({
        "nama": "LED, HID & Bohlam",
        "created_by": 1
    })
    Category.post({
        "nama": "Visor & Windshield",
        "created_by": 1
    })
    Category.post({
        "nama": "Oil & Fluids",
        "created_by": 1
    })
    Category.post({
        "nama": "Cleaner & Detailing",
        "created_by": 1
    })
    Category.post({
        "nama": "Helm",
        "created_by": 1
    })
    Category.post({
        "nama": "Tas",
        "created_by": 1
    })
    Category.post({
        "nama": "Gloves",
        "created_by": 1
    })
    Category.post({
        "nama": "Jas Hujan",
        "created_by": 1
    })
    print("Seed (category) successfully created!")
