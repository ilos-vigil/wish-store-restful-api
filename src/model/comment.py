import datetime
import utils
from sqlalchemy import func, and_
from model import db
from model.comment_reply import CommentReply
from error import *



class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_post = db.Column(db.Integer, nullable=True)
    pesan = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user = db.relationship('User', primaryjoin='foreign(Comment.created_by) == User.id')

    def __init__(self, json):
        self.id_post = json['id_post']
        self.pesan = json['pesan']
        self.created_by = json['created_by']

    def to_json(self):
        comment_reply = db.session.query(CommentReply).filter(and_(CommentReply.id_comment == self.id, CommentReply.is_deleted == False)).all()
        list_comment_reply = []
        for cr in comment_reply:
            list_comment_reply.append(cr.to_json())

        return {
            'id': self.id,
            'id_post': self.id_post,
            'comment_reply': list_comment_reply,
            'pesan': self.pesan,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'user': self.user.to_json_min(),
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        comment = None
        if json is None or len(json.keys()) == 0:
            comment = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            comment = cls.query.get(json['id'])
        elif 'id_post' in json:
            comment = cls.query.filter_by(is_deleted=0, id_post=json['id_post']).all()
        elif 'user_friends_id' in json:
            current_time = datetime.datetime.utcnow()
            three_days_ago = current_time - datetime.timedelta(days=3)
            comment = db.session.query(Comment).filter(and_(Comment.created_by.in_(json['user_friends_id']), Comment.created_at > current_time - three_days_ago)).all()
        else:
            raise InvalidGETKeyError
        
        if comment is None or (type(comment) is not list and comment.is_deleted == 1):
            raise ItemNotExistError
        else:
            return comment

    @classmethod
    def post(cls, json):
        comment = Comment(json)
        db.session.add(comment)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            comment = cls.query.get(json['id'])
            if comment is None or comment.is_deleted == 1:
                raise ItemNotExistError
            else:
                comment.pesan = json['pesan'] if 'pesan' in json else comment.pesan

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            comment = cls.query.get(json['id'])
            if comment is None or comment.is_deleted == 1:
                raise ItemNotExistError
            else:
                comment.is_deleted = 1
                comment.deleted_by = json['deleted_by']
                comment.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
