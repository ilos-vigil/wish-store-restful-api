import utils
from sqlalchemy import func
from model import db
from error import *


class Wishlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, nullable=True)
    id_post = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)

    post_info = db.relationship('Post', primaryjoin='foreign(Wishlist.id_post) == Post.id')

    __table_args__ = (
        db.UniqueConstraint('id_user', 'id_post', name='constraint_user_post'),
    )

    def __init__(self, json):
        self.id_user = json['created_by']
        self.id_post = json['id_post']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_user': self.id_user,
            'id_post': self.id_post,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'post': self.post_info.to_json(),
        }

    @classmethod
    def get(cls, json):
        wishlist = None
        if json is None or len(json.keys()) == 0:
            wishlist = cls.query.all()
        elif 'id' in json:
            wishlist = cls.query.get(json['id'])
        elif 'id_user' in json:
            wishlist = cls.query.filter_by(created_by=json['id_user']).all()
        else:
            raise InvalidGETKeyError

        if wishlist is None:
            raise ItemNotExistError
        return wishlist

    @classmethod
    def post(cls, json):
        wishlist = Wishlist(json)
        db.session.add(wishlist)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            wishlist = cls.query.get(json['id'])
            if wishlist is None:
                raise ItemNotExistError
            else:
                wishlist.comment = json['comment'] if 'comment' in json else wishlist.comment
                wishlist.rating = json['rating'] if 'rating' in json else wishlist.rating

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            wishlist = cls.query.get(json['id'])
            if wishlist is None:
                raise ItemNotExistError
            else:
                db.session.delete(wishlist)
                db.session.commit()
        elif 'id_post' in json and 'id_user' in json:
            wishlist = cls.query.filter_by(id_post=json['id_post'], id_user=json['id_user']).first()
            if wishlist is None:
                raise ItemNotExistError
            else:
                db.session.delete(wishlist)
                db.session.commit()
        else:
            raise MissingIDKeyError
