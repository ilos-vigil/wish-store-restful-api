import utils
from sqlalchemy import func
from model import db
from error import *
import secrets
from flask_bcrypt import generate_password_hash, check_password_hash


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_onesignal = db.Column(db.String(255), nullable=True)
    nama = db.Column(db.String(255), nullable=True)
    gender = db.Column(db.Boolean, nullable=True)
    username = db.Column(db.String(255), nullable=True, index=True, unique=True)
    password = db.Column(db.String(255), nullable=True)
    no_rekening = db.Column(db.String(255), nullable=True)
    token = db.Column(db.String(256), nullable=True, index=True)
    is_admin = db.Column(db.Boolean, nullable=True)
    alamat = db.Column(db.String(255), nullable=True)
    kota = db.Column(db.String(255), nullable=True)
    status = db.Column(db.Integer, nullable=True, server_default='0')
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    def __init__(self, json):
        self.id_onesignal = json['id_onesignal'] if 'id_onesignal' in json else None
        self.nama = json['nama'] if 'nama' in json else None
        self.gender = json['gender'] if 'gender' in json else None
        self.username = json['username']
        self.password = generate_password_hash(json['password']).decode('utf8')
        self.no_rekening = json['no_rekening']
        # self.token = json['token'] if 'token' in json else None
        self.is_admin = json['is_admin'] if 'is_admin' in json else None
        self.alamat = json['alamat'] if 'alamat' in json else None
        self.kota = json['kota'] if 'kota' in json else None
        self.created_by = json['created_by'] if 'created_by' in json else None

    def to_json(self):
        return {
            'id': self.id,
            'id_onesignal': self.id_onesignal,
            'nama': self.nama,
            'gender': self.gender,
            'username': self.username,
            'password': self.password,
            'no_rekening': self.no_rekening,
            'token': self.token,
            'is_admin': self.is_admin,
            'alamat': self.alamat,
            'kota': self.kota,
            'status': self.status,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    def to_json_basic(self):
        return {
            'id': self.id,
            'id_onesignal': self.id_onesignal,
            'nama': self.nama,
            'gender': self.gender,
            'token': self.token,
            'username': self.username,
            'no_rekening': self.no_rekening,
            'is_admin': self.is_admin,
            'alamat': self.alamat,
            'kota': self.kota,
            'status': self.status,
        }

    def to_json_min(self):
        return {
            'id': self.id,
            'id_onesignal': self.id_onesignal,
            'nama': self.nama,
            'gender': self.gender,
            'username': self.username,
            'no_rekening': self.no_rekening,
            'alamat': self.alamat,
            'kota': self.kota,
            'status': self.status,
        }

    @classmethod
    def get(cls, json):
        user = None
        if json is None or len(json.keys()) == 0:
            user = cls.query.filter_by(is_deleted=0).all()
        elif 'username' in json:
            user = cls.query.filter_by(username=json['username']).first()
        elif 'token' in json:
            user = cls.query.filter_by(token=json['token']).first()
        elif 'id' in json:
            user = cls.query.get(json['id'])
        else:
            raise InvalidGETKeyError

        if user is None or (type(user) is not list and user.is_deleted == 1):
            raise ItemNotExistError
        else:
            return user

    @classmethod
    def post(cls, json):
        user = User(json)
        db.session.add(user)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            user = cls.query.get(json['id'])
            if user is None or user.is_deleted == 1:
                raise ItemNotExistError
            else:
                user.nama = json['nama'] if 'nama' in json else user.nama
                user.gender = json['gender'] if 'gender' in json else user.gender
                user.password = json['password'] if 'password' in json else user.password
                user.alamat = json['alamat'] if 'alamat' in json else user.alamat
                user.kota = json['kota'] if 'kota' in json else user.kota
                user.status = json['status'] if 'status' in json else user.status

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            user = cls.query.get(json['id'])
            if user is None or user.is_deleted == 1:
                raise ItemNotExistError
            else:
                user.is_deleted = 1
                user.deleted_by = json['deleted_by']
                user.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def register(cls, json):
        user = User(json)
        db.session.add(user)
        db.session.commit()
        return user

    @classmethod
    def check_username_exist(cls, json):
        if 'username' in json:
            user = cls.query.filter_by(username=json['username']).first()

            if user is None:
                return False
            return True
        else:
            raise KeyError("username and password required")

    @classmethod
    def check_token_exist(cls, token):
        user = cls.query.filter_by(token=token).first()
        if user is None or user.is_deleted == 1:
            return False
        else:
            return True

    def check_password(self, password):
        return check_password_hash(self.password, password)
