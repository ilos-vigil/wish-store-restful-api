import utils
from datetime import datetime
from sqlalchemy import func
from model import db
from error import *



class Deal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_post = db.Column(db.Integer, nullable=True)
    id_poster = db.Column(db.Integer, nullable=True)
    id_penawar = db.Column(db.Integer, nullable=True)
    judul = db.Column(db.String(255), nullable=True)
    keterangan = db.Column(db.String(255), nullable=True)
    url_photo = db.Column(db.String(255), nullable=True)
    harga = db.Column(db.Integer, nullable=True)
    status = db.Column(db.Integer, server_default='0')
    accepted_at = db.Column(db.DateTime, nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    post_data = db.relationship('Post', primaryjoin='foreign(Deal.id_post) == Post.id')
    user_poster = db.relationship('User', primaryjoin='foreign(Deal.id_poster) == User.id')
    user_penawar = db.relationship('User', primaryjoin='foreign(Deal.id_penawar) == User.id')

    def __init__(self, json):
        self.id_post = json['id_post']
        self.id_poster = json['id_poster']
        self.id_penawar = json['id_penawar']
        self.judul = json['judul']
        self.keterangan = json['keterangan']
        self.harga = json['harga']
        self.created_by = json['created_by']

    def to_json(self):
        if self.url_photo is None:
            string_url_photo = "https://via.placeholder.com/150?text=No+Image+Available"
        elif 'http' in self.url_photo or 'www' in self.url_photo:
            string_url_photo = self.url_photo
        else:
            string_url_photo = utils.WISH_DOMAIN + self.url_photo

        return {
            'id': self.id,
            'id_post': self.id_post,
            'post': self.post_data.to_json(),
            'id_poster': self.id_poster,
            'user_poster': self.user_poster.to_json_min(),
            'id_penawar': self.id_penawar,
            'user_penawar': self.user_penawar.to_json_min(),
            'judul': self.judul,
            'keterangan': self.keterangan,
            'url_photo': string_url_photo,
            'harga': self.harga,
            'status': self.status,
            'accepted_at': self.accepted_at if self.accepted_at is None else datetime.timestamp(self.accepted_at),
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        deal = None
        if json is None or len(json.keys()) == 0:
            deal = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            deal = cls.query.get(json['id'])
        elif 'id_poster' in json:
            deal = cls.query.filter_by(is_deleted=0, id_poster=json['id_poster']).all()
        elif 'id_penawar' in json:
            deal = cls.query.filter_by(is_deleted=0, id_penawar=json['id_penawar']).all()
        else:
            raise InvalidGETKeyError

        if deal is None or (type(deal) is not list and deal.is_deleted == 1):
            raise ItemNotExistError
        else:
            return deal

    @classmethod
    def post(cls, json):
        deal = Deal(json)
        db.session.add(deal)
        db.session.commit()

        if 'base64' in json:
            utils.base64_to_file(cls.__tablename__, deal.id, json['base64'])
            deal.url_photo = f'static/image/{cls.__tablename__}/{deal.id}.jpg'
            db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            deal = cls.query.get(json['id'])
            if deal is None or deal.is_deleted == 1:
                raise ItemNotExistError
            else:
                deal.judul = json['judul'] if 'judul' in json else deal.judul
                deal.keterangan = json['keterangan'] if 'keterangan' in json else deal.keterangan
                deal.url_photo = json['url_photo'] if 'url_photo' in json else deal.url_photo
                deal.status = json['status'] if 'status' in json else deal.status
                deal.harga = json['harga'] if 'harga' in json else deal.harga
                deal.accepted_at = json['accepted_at'] if 'accepted_at' in json else deal.accepted_at

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            deal = cls.query.get(json['id'])
            if deal is None or deal.is_deleted == 1:
                raise ItemNotExistError
            else:
                deal.is_deleted = 1
                deal.deleted_by = json['deleted_by']
                deal.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
