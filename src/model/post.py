import datetime
import utils
from sqlalchemy import func, and_
from model import db
from model.wishlist import Wishlist
from model.comment import Comment
from model.user_review import UserReview


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_category = db.Column(db.Integer, nullable=True)
    judul = db.Column(db.String(255), nullable=True)
    deskripsi = db.Column(db.String(6000), nullable=True)
    url_photo = db.Column(db.String(255), nullable=True)
    url_video = db.Column(db.String(255), nullable=True)
    user_alamat = db.Column(db.String(255), nullable=True)
    user_kota = db.Column(db.String(255), nullable=True)
    status = db.Column(db.Integer, nullable=True, server_default='0')
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    category = db.relationship('Category', primaryjoin='foreign(Post.id_category) == Category.id')
    user_created = db.relationship('User', primaryjoin='foreign(Post.created_by) == User.id')

    def __init__(self, json):
        self.id_category = json['id_category']
        self.judul = json['judul']
        self.deskripsi = json['deskripsi']
        self.url_photo = json['url_photo'] if 'url_photo' in json else None
        self.url_video = json['url_video'] if 'url_video' in json else None
        self.user_alamat = json['user_alamat'] if 'user_alamat' in json else None
        self.user_kota = json['user_kota'] if 'user_kota' in json else None
        self.created_by = json['created_by']

    def to_json(self):
        if self.url_photo is None:
            string_url_photo = None
        elif 'http' in self.url_photo or 'www' in self.url_photo:
            string_url_photo = self.url_photo
        else:
            string_url_photo = utils.WISH_DOMAIN + self.url_photo

        wishlist_count = db.session.query(Wishlist).filter(Wishlist.id_post == self.id).count()
        comment_count = db.session.query(Comment).filter(Comment.id_post == self.id).count()
        rating_average = db.session.query(func.avg(UserReview.rating).label('rating_average')).filter(UserReview.id_target == self.created_by).first().rating_average

        return {
            'id': self.id,
            'id_category': self.id_category,
            'category': self.category.to_json(),
            'judul': self.judul,
            'deskripsi': self.deskripsi,
            'url_photo': string_url_photo,
            'url_video': self.url_video,
            'user_alamat': self.user_alamat,
            'user_kota': self.user_kota,
            'status': self.status,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'user_created': self.user_created.to_json_min(),
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
            'wishlist_count': wishlist_count,
            'comment_count': comment_count,
            'rating_average': float(rating_average) if rating_average is not None else -1.0,
        }

    def to_json_is_wishlist(self, id_user):
        json = self.to_json()
        json['is_wishlist'] = False if db.session.query(Wishlist).filter(and_(Wishlist.id_post == self.id, Wishlist.id_user == id_user)).count() == 0 else True

        return json

    @classmethod
    def get(cls, json):
        if json is None or len(json.keys()) == 0:
            post = cls.query.filter_by(is_deleted=0, status=0).all()
            if post is None:
                raise ItemNotExistError
            else:
                return post
        elif 'id' in json:
            post = cls.query.get(json['id'])
            if post is None or post.is_deleted == 1:
                raise ItemNotExistError
            else:
                return post
        elif 'page_number' in json and 'keyword' in json:
            post = cls.query.filter(and_(cls.is_deleted == 0, cls.status == 0, cls.judul.like(f'%{json["keyword"]}%'))).paginate(json['page_number'], per_page=10)
            if post is None:
                raise ItemNotExistError
            else:
                return post.items
        elif 'page_number' in json:
            post = cls.query.filter_by(is_deleted=0, status=0).paginate(json['page_number'], per_page=10)
            if post is None:
                raise ItemNotExistError
            else:
                return post.items
        elif 'user_friends_id' in json:
            current_time = datetime.datetime.utcnow()
            three_days_ago = current_time - datetime.timedelta(days=3)
            post_feed = db.session.query(Post).filter(and_(Post.created_by.in_(json['user_friends_id']), Post.created_at > current_time - three_days_ago)).all()
            if post_feed is None:
                raise ItemNotExistError
            else:
                return post_feed
        elif 'id_user' in json and json['on_wishlist']:
            post_wishlist = db.session.query(Post, Wishlist).filter(Post.id == Wishlist.id_post, Wishlist.id_user == json['id_user'], Post.is_deleted == False, Wishlist.is_deleted == False).all()
            if post_wishlist is None:
                raise ItemNotExistError
            else:
                post_list = []
                for pw in post_wishlist:
                    post_list.append(pw.Post.to_json())
                return post_list
        elif 'id_user' in json and not json['on_wishlist'] and 'status' in json:
            post = cls.query.filter_by(is_deleted=0, created_by=json['id_user'], status=json['status']).all()
            if post is None:
                raise ItemNotExistError
            else:
                return post
        elif 'id_user' in json and not json['on_wishlist']:
            post = cls.query.filter_by(is_deleted=0, created_by=json['id_user']).all()
            if post is None:
                raise ItemNotExistError
            else:
                return post
        else:
            raise InvalidGETKeyError

    @classmethod
    def post(cls, json):
        post = Post(json)
        db.session.add(post)
        db.session.commit()

        if 'base64' in json:
            utils.base64_to_file(cls.__tablename__, post.id, json['base64'])
            post.url_photo = f'static/image/{cls.__tablename__}/{post.id}.jpg'
            db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            post = cls.query.get(json['id'])
            if post is None or post.is_deleted == 1:
                raise ItemNotExistError
            else:
                post.id_category = json['id_category'] if 'id_category' in json else post.id_category
                post.judul = json['judul'] if 'judul' in json else post.judul
                post.deskripsi = json['deskripsi'] if 'deskripsi' in json else post.deskripsi
                post.url_photo = json['url_photo'] if 'url_photo' in json else post.url_photo
                post.url_video = json['url_video'] if 'url_video' in json else post.url_video
                post.user_alamat = json['user_alamat'] if 'user_alamat' in json else post.user_alamat
                post.user_kota = json['user_kota'] if 'user_kota' in json else post.user_kota
                post.status = json['status'] if 'status' in json else post.status

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            post = cls.query.get(json['id'])
            if post is None or post.is_deleted == 1:
                raise ItemNotExistError
            else:
                post.is_deleted = 1
                post.deleted_by = json['deleted_by']
                post.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
