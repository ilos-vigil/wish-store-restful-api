from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

# intentionally located here to prevent cyclic dependency
from .category import *
from .chat import *
from .comment_reply import *
from .comment import *
from .deal import *
from .post import *
from .report_post import *
from .report_user import *
from .trans import *
from .user_block import *
from .user_friend import *
from .user_review import *
from .user import *
