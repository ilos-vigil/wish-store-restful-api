import utils
from sqlalchemy import func, and_
from model import db
from model.user import User


class ReportPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_post = db.Column(db.Integer, nullable=True)
    id_user = db.Column(db.Integer, nullable=True)
    keterangan = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    post_data = db.relationship('Post', primaryjoin='foreign(ReportPost.id_post) == Post.id')
    user = db.relationship('User', primaryjoin='foreign(ReportPost.id_user) == User.id')

    def __init__(self, json):
        self.id_post = json['id_post']
        self.id_user = json['id_user']
        self.keterangan = json['keterangan']
        self.created_by = json['created_by']

    def to_json(self):
        post_user = db.session.query(User).filter(and_(User.id == self.post_data.created_by, self.post_data.id == self.id_post)).first()

        return {
            'id': self.id,
            'id_post': self.id_post,
            'post': self.post_data.to_json(),
            'post_user': post_user.to_json_min(),
            'id_user': self.id_user,
            'user': self.user.to_json_min(),
            'keterangan': self.keterangan,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        report_post = None
        if json is None or len(json.keys()) == 0:
            report_post = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            report_post = cls.query.get(json['id'])
        else:
            raise InvalidGETKeyError
        
        if report_post is None or (type(report_post) is not list and report_post.is_deleted == 1):
            raise ItemNotExistError
        return report_post

    @classmethod
    def post(cls, json):
        report_post = ReportPost(json)
        db.session.add(report_post)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            report_post = cls.query.get(json['id'])
            if report_post is None or report_post.is_deleted == 1:
                raise ItemNotExistError
            else:
                report_post.keterangan = json['keterangan'] if 'keterangan' in json else report_post.keterangan

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            report_post = cls.query.get(json['id'])
            if report_post is None or report_post.is_deleted == 1:
                raise ItemNotExistError
            else:
                report_post.is_deleted = 1
                report_post.deleted_by = json['deleted_by']
                report_post.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
