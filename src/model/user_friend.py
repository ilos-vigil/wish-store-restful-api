import utils
from sqlalchemy import func, and_
from model import db
from error import *



class UserFriend(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, nullable=True)
    id_target = db.Column(db.Integer, nullable=True)
    is_accepted = db.Column(db.Boolean, nullable=True, server_default='0')
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    user_id_user = db.relationship('User', primaryjoin='foreign(UserFriend.id_user) == User.id')
    user_id_target = db.relationship('User', primaryjoin='foreign(UserFriend.id_target) == User.id')

    __table_args__ = (
        db.UniqueConstraint('id_user', 'id_target', name='constraint_user_target'),
    )

    def __init__(self, json):
        self.id_user = json['id_user']
        self.id_target = json['id_target']
        self.created_by = json['created_by']

    def to_json(self):
        return {
            'id': self.id,
            'id_user': self.id_user,
            'user_id_user': self.user_id_user.to_json_min(),
            'id_target': self.id_target,
            'user_id_target': self.user_id_target.to_json_min(),
            'is_accepted': self.is_accepted,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        user_friend = None
        if json is None or len(json.keys()) == 0:
            user_friend = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            user_friend = cls.query.get(json['id'])
        elif 'id_user' in json and 'is_accepted' in json:
            user_friend = cls.query.filter_by(is_deleted=0, id_user=json['id_user'], is_accepted=json['is_accepted']).all()
        elif 'id_target' in json and 'is_accepted' in json:
            user_friend = cls.query.filter_by(is_deleted=0, id_target=json['id_target'], is_accepted=json['is_accepted']).all()
        elif 'id_user_target' in json and 'is_accepted' in json:            
            user_friend = cls.query.filter(
                        (and_(cls.id_user == json['id_user_target'], cls.is_deleted == 0, cls.is_accepted == json['is_accepted'] )) |
                        (and_(cls.id_target == json['id_user_target'], cls.is_deleted == 0, cls.is_accepted == json['is_accepted'] ))
                    ).all()
        else:
            raise InvalidGETKeyError


        if user_friend is None or (type(user_friend) is not list and user_friend.is_deleted == 1):
            raise ItemNotExistError
        return user_friend

    @classmethod
    def post(cls, json):
        user_friend = UserFriend(json)
        db.session.add(user_friend)
        db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            user_friend = cls.query.get(json['id'])
            if user_friend is None or user_friend.is_deleted == 1:
                raise ItemNotExistError
            else:
                user_friend.is_accepted = json['is_accepted'] if 'is_accepted' in json else user_friend.is_accepted

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            user_friend = cls.query.get(json['id'])
            if user_friend is None or user_friend.is_deleted == 1:
                raise ItemNotExistError
            else:
                db.session.delete(user_friend)
                db.session.commit()
        elif 'id_target' in json and 'id_user' in json:
            user_friend = cls.query.filter_by(id_target=json['id_target'], id_user=json['id_user']).first()
            if user_friend is None:
                raise ItemNotExistError
            else:
                db.session.delete(user_friend)
                db.session.commit()
        else:
            raise MissingIDKeyError
