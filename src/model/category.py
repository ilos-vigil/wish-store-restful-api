import utils
from sqlalchemy import func
from model import db
from error import *


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(255), nullable=True, index=True, unique=True)
    url_photo = db.Column(db.String(255), nullable=True)
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    def __init__(self, json):
        self.nama = json['nama']
        self.created_by = json['created_by']

    def to_json(self):
        if self.url_photo is None:
            string_url_photo = "https://via.placeholder.com/250x150/000000/FFFFFF/?text=+"
        elif 'http' in self.url_photo or 'www' in self.url_photo:
            string_url_photo = self.url_photo
        else:
            string_url_photo = utils.WISH_DOMAIN + self.url_photo

        return {
            'id': self.id,
            'nama': self.nama,
            'url_photo': string_url_photo,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        category = None
        if json is None or len(json.keys()) == 0:
            category = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            category = cls.query.get(json['id'])
        elif 'nama' in json:
            category = cls.query.filter_by(is_deleted=0, nama=json['nama']).first()
        else:
            raise InvalidGETKeyError

        if category is None or (type(category) is not list and category.is_deleted == 1):
            raise ItemNotExistError
        return category

    @classmethod
    def post(cls, json):
        category = Category(json)
        db.session.add(category)
        db.session.commit()

        if 'base64' in json:
            utils.base64_to_file(cls.__tablename__,
                              category.id, json['base64'])
            category.url_photo = f'static/image/{cls.__tablename__}/{category.id}.jpg'
            db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            category = cls.query.get(json['id'])
            if category is None or category.is_deleted == 1:
                raise ItemNotExistError
            else:
                category.nama = json['nama'] if 'nama' in json else category.nama
                if 'base64' in json:
                    utils.base64_to_file(cls.__tablename__, category.id, json['base64'])
                    category.url_photo = f'static/image/{cls.__tablename__}/{category.id}.jpg'

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            category = cls.query.get(json['id'])
            if category is None or category.is_deleted == 1:
                raise ItemNotExistError
            else:
                category.is_deleted = 1
                category.deleted_by = json['deleted_by']
                category.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
