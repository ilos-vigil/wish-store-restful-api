import utils
from sqlalchemy import func, and_
from model import db
from model.deal import Deal
from error import *


class Trans(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_deal = db.Column(db.Integer, nullable=True)
    url_photo = db.Column(db.String(255), nullable=True)
    keterangan = db.Column(db.String(255), nullable=True)
    alamat = db.Column(db.String(255), nullable=True)
    kota = db.Column(db.String(255), nullable=True)
    status = db.Column(db.Integer, nullable=True, server_default='0')
    created_at = db.Column(db.DateTime, server_default=func.now())
    created_by = db.Column(db.Integer, nullable=True)
    deleted_at = db.Column(db.DateTime, nullable=True)
    deleted_by = db.Column(db.Integer, nullable=True)
    is_deleted = db.Column(db.Boolean, server_default='0')

    deal = db.relationship('Deal', primaryjoin='foreign(Trans.id_deal) == Deal.id')

    def __init__(self, json):
        self.id_deal = json['id_deal']
        self.keterangan = json['keterangan']
        self.alamat = json['alamat']
        self.kota = json['kota']
        self.created_by = json['created_by']

    def to_json(self):
        if self.url_photo is None:
            string_url_photo = "https://via.placeholder.com/150?text=No+Image+Available"
        elif 'http' in self.url_photo or 'www' in self.url_photo:
            string_url_photo = self.url_photo
        else:
            string_url_photo = utils.WISH_DOMAIN + self.url_photo

        return {
            'id': self.id,
            'id_deal': self.id_deal,
            'deal': self.deal.to_json(),
            'url_photo': string_url_photo,
            'keterangan': self.keterangan,
            'alamat': self.alamat,
            'kota': self.kota,
            'status': self.status,
            'created_at': self.created_at.strftime("%d %B %Y %H:%M"),
            'created_by': self.created_by,
            'deleted_at': self.deleted_at if self.deleted_at is None else self.deleted_at.strftime("%d %B %Y %H:%M"),
            'deleted_by': self.deleted_by,
            'is_deleted': self.is_deleted,
        }

    @classmethod
    def get(cls, json):
        trans = None
        if json is None or len(json.keys()) == 0:
            trans = cls.query.filter_by(is_deleted=0).all()
        elif 'id' in json:
            trans = cls.query.get(json['id'])
        elif 'id_penerima' in json:
            trans = cls.query.filter_by(is_deleted=0, created_by=json['id_penerima']).all()
        elif 'id_penawar' in json:
            trans = db.session.query(Trans).filter(and_(Trans.is_deleted == 0, Trans.id_deal == Deal.id, Deal.id_penawar == json['id_penawar'])).all()
        else:
            raise InvalidGETKeyError

        if trans is None or (type(trans) is not list and trans.is_deleted == 1):
            raise ItemNotExistError
        return trans

    @classmethod
    def post(cls, json):
        trans = Trans(json)
        db.session.add(trans)
        db.session.commit()

        Deal.patch({
            'id': trans.id_deal,
            'status': 1
        })

        if 'base64' in json:
            utils.base64_to_file(cls.__tablename__, trans.id, json['base64'])
            trans.url_photo = f'static/image/{cls.__tablename__}/{trans.id}.jpg'
            db.session.commit()

    @classmethod
    def patch(cls, json):
        if 'id' in json:
            trans = cls.query.get(json['id'])
            if trans is None or trans.is_deleted == 1:
                raise ItemNotExistError
            else:
                trans.url_photo = json['url_photo'] if 'url_photo' in json else trans.url_photo
                trans.keterangan = json['keterangan'] if 'keterangan' in json else trans.keterangan
                trans.alamat = json['alamat'] if 'alamat' in json else trans.alamat
                trans.kota = json['kota'] if 'kota' in json else trans.kota
                trans.status = json['status'] if 'status' in json else trans.status

                if 'base64' in json:
                    utils.base64_to_file(cls.__tablename__, trans.id, json['base64'])
                    trans.url_photo = f'static/image/{cls.__tablename__}/{trans.id}.jpg'
                    db.session.commit()

                db.session.commit()
        else:
            raise MissingIDKeyError

    @classmethod
    def delete(cls, json):
        if 'id' in json:
            trans = cls.query.get(json['id'])
            if trans is None or trans.is_deleted == 1:
                raise ItemNotExistError
            else:
                trans.is_deleted = 1
                trans.deleted_by = json['deleted_by']
                trans.deleted_at = func.now()
                db.session.commit()
        else:
            raise MissingIDKeyError
