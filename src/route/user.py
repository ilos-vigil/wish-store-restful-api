from controller.user import (
    # feed_controller.py
    FeedController,
)


def init_app(api):
    '''
    User specific action
    '''

    api.add_resource(FeedController, '/feed')

    # Move to user

    return api
