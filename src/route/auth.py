from controller.auth import (
    RegisterController,
    LoginController
)


def init_app(api):
    api.add_resource(RegisterController, '/register')
    api.add_resource(LoginController, '/login')

    return api
