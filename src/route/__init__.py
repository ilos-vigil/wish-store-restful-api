from . import auth, crud, static, user


def init_app(api):
    api = auth.init_app(api)
    api = crud.init_app(api)
    api = static.init_app(api)
    api = user.init_app(api)

    return api
