from controller.static import (
    AboutController,
    StaticImageController
)


def init_app(api):
    api.add_resource(AboutController, '/')
    api.add_resource(StaticImageController, '/static/image/<string:model>/<string:file_name>')

    return api
