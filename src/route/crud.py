from controller.crud import (
    # category_controller
    CategoryController,
    CategoryListController,
    # chat_controller
    ChatController,
    ChatListController,
    ChatRecentController,
    ChatHistoryController,
    # comment_controller
    CommentController,
    CommentListController,
    CommentPostListController,
    # comment_reply_controller
    CommentReplyController,
    CommentReplyListController,
    # deal_controller
    DealController,
    DealListController,
    # trans_controller
    TransController,
    TransListController,
    # post_controller
    PostController,
    PostListController,
    PostUserListController,
    PostPaginationController,
    PostWishlistedController,
    PostListUserController,
    # report_post_controller
    ReportPostController,
    ReportPostListController,
    # report_user_controller
    ReportUserController,
    ReportUserListController,
    # user_controller
    UserController,
    UserListController,
    # user_block_controller
    UserBlockController,
    UserBlockListController,
    # user_friend_controller
    UserFriendController,
    UserFriendListController,
    # user_review_controller
    UserReviewController,
    UserReviewListController,
    EachUserReviewController,
    # wishlist_controller
    WishlistController,
    WishlistListController,
)


def init_app(api):
    '''
    CRUD operation for all tables
    '''

    api.add_resource(CategoryListController, '/category')
    api.add_resource(CategoryController, '/category/<int:id>')

    api.add_resource(ChatListController, '/chat')
    api.add_resource(ChatController, '/chat/<int:id>')
    api.add_resource(ChatRecentController, '/chat/recent/<int:id_user_a>/<int:id_user_b>')  # most recent chat with other user
    api.add_resource(ChatHistoryController, '/chat/history/<int:id_user_a>/<int:id_user_b>')  # full chat history with specific user

    api.add_resource(CommentListController, '/comment')
    api.add_resource(CommentController, '/comment/<int:id>')
    api.add_resource(CommentPostListController, '/comment/post/<int:id_post>')  # comments from specific post

    api.add_resource(CommentReplyListController, '/commentreply')
    api.add_resource(CommentReplyController, '/commentreply/<int:id>')

    api.add_resource(DealListController, '/deal')
    api.add_resource(DealController, '/deal/<int:id>')

    api.add_resource(TransListController, '/trans')
    api.add_resource(TransController, '/trans/<int:id>')

    api.add_resource(PostListController, '/post')
    api.add_resource(PostController, '/post/<int:id>')
    api.add_resource(PostUserListController, '/post/user/<int:id_user>')  # posts from specific user
    api.add_resource(PostPaginationController, '/post/page/<int:page_number>/<string:keyword>')  # pagination post search
    api.add_resource(PostListUserController, '/user/<int:id_user>/post/')  # post made by user
    api.add_resource(PostWishlistedController, '/user/<int:id_user>/post/wishlist')  # post wishlisted by user

    api.add_resource(ReportPostListController, '/reportpost')
    api.add_resource(ReportPostController, '/reportpost/<int:id>')

    api.add_resource(ReportUserListController, '/reportuser')
    api.add_resource(ReportUserController, '/reportuser/<int:id>')

    api.add_resource(UserListController, '/user')
    api.add_resource(UserController, '/user/<int:id>')

    api.add_resource(UserBlockListController, '/userblock')
    api.add_resource(UserBlockController, '/userblock/<int:id>')

    api.add_resource(UserFriendListController, '/userfriend')
    api.add_resource(UserFriendController, '/userfriend/<int:id>')

    api.add_resource(UserReviewListController, '/userreview')
    api.add_resource(UserReviewController, '/userreview/<int:id>')
    api.add_resource(EachUserReviewController, '/userreview/user/<int:id_target>')  # review made by specific user

    api.add_resource(WishlistListController, '/wishlist')
    api.add_resource(WishlistController, '/wishlist/<int:id>')

    return api
