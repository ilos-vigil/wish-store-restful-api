from flask_restful import abort, request
from functools import wraps
from model.user import User


def check_authentication(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        token = request.headers.get('token')
        if token is None:
            abort(401, message='Token is required')
        elif User.check_token_exist(token):
            return func(*args, **kwargs)
        else:
            abort(401, message='Token is invalid')
    return decorator
