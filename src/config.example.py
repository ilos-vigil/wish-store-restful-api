class Config():
    UPLOAD_FOLDER = './image'
    MAX_CONTENT_LENGTH = 50 * 1024 * 1024
    JWT_SECRET_KEY = 'REPLACE_WITH_YOUR_SECRET_KEY'


class Development(Config):
    SQLALCHEMY_ECHO = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wishstore:Test123!@172.17.0.3/wishstore'
    SERVER_NAME = '172.17.0.6:80'


class Deployment(Config):
    pass
