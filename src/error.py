class InvalidGETKeyError(Exception):
    pass


class MissingIDKeyError(Exception):
    pass


class ItemNotExistError(Exception):
    pass


errors = {
    'InvalidGETKeyError': {
        'message': "GET with such key is not available!",
        'status': 403,
    },
    'MissingIDKeyError': {
        'message': "Item ID is required!",
        'status': 403,
    },
    'ItemNotExistError': {
        'message': "Item is not exist or deleted!",
        'status': 404,
    },
}
