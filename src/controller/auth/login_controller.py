from flask_restful import Resource, request, abort
from flask_jwt_extended import create_access_token
from model import User
import datetime


class LoginController(Resource):
    def post(self):
        try:
            json = request.get_json()
            user = User.get(json)
            authorized = User.check_password(user, json['password'])

            if not authorized:
                return {'error': 'Email or password invalid'}, 401

            expires = datetime.timedelta(days=7)
            access_token = create_access_token(identity=str(user.id), expires_delta=expires)
            return {'token': access_token}, 200
        except Exception as err:
            abort(404, message=err.args)
