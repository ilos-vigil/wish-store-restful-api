from flask_restful import Resource, request, abort
from model import User


class RegisterController(Resource):
    def post(self):
        try:
            json = request.get_json()
            user = User.register(json).to_json_basic()
            user['message'] = "Sukses register"
            return user, 201
        except Exception as err:
            abort(500, message=err.args)
