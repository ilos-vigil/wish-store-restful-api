import datetime
import utils
from flask_restful import Resource, request, abort
from flask_jwt_extended import jwt_required
from model import db
from sqlalchemy.exc import DataError
from binascii import Error
from error import ItemNotExistError
import pymysql


class BaseController(Resource):
    method_decorators = [jwt_required]

    def get(self, ModelClass, **dictionary):
        try:
            data = ModelClass.get(dictionary)

            if type(data) is list:
                json = []
                for d in data:
                    json.append(d.to_json())
                return json
            elif isinstance(data, db.Model):
                return data.to_json()
            else:
                json = []
                for row in data:
                    dicts = {}
                    for key, value in row.items():
                        if type(value) is datetime.datetime and value is not None:
                            dicts[key] = value.strftime("%d %B %Y %H:%M")
                        else:
                            dicts[key] = value
                    json.append(dicts)
                return json
        except ItemNotExistError:
            return []


    def patch(self, ModelClass, id):
        json = request.get_json()
        json['id'] = id
        ModelClass.patch(json)
        return {'message': f'{ModelClass.__tablename__} changed'}, 200

    def delete(self, ModelClass, id):
        json = {'id': id}
        json['deleted_by'] = utils.get_user_id(token=request.headers.get('token'))
        ModelClass.delete(json)
        return {'message': f'{ModelClass.__tablename__} deleted'}, 200


class BaseListController(Resource):
    method_decorators = [jwt_required]

    def get(self, ModelClass, **dictionary):
        try:
            data = None
            if request.get_json() is None:
                data = ModelClass.get(request.get_json())
            else:
                data = ModelClass.get({**dictionary, **request.get_json()})

            if type(data) is list:
                json = []
                for d in data:
                    if isinstance(d, dict):
                        json.append(d)
                    else:
                        json.append(d.to_json())
                return json
            elif isinstance(data, db.Model):
                return data.to_json()
            else:
                json = []
                for row in data:
                    dicts = {}
                    for key, value in row.items():
                        if type(value) is datetime.datetime and value is not None:
                            dicts[key] = value.strftime("%d %B %Y %H:%M")
                        else:
                            dicts[key] = value
                    json.append(dicts)
                return json        
        except ItemNotExistError:
            return []
        except pymysql.Error as err:
            abort(500, message=err.args)

    def post(self, ModelClass):
        try:
            json = request.get_json()
            json['created_by'] = utils.get_user_id(token=request.headers.get('token'))
            ModelClass.post(json)
            return {'message': f'{ModelClass.__tablename__} created'}, 201
        except TypeError as err:
            abort(406, message="Require data with media-type application/json")
        except KeyError as err:
            abort(406, message=f"Key {err.args} is required")
        except DataError as err:
            abort(406, message=err.args)
        except Error as err:
            abort(406, message="Invalid Base64")
        except Exception as err:
            abort(500, message=err.args)


# Class below are duplicate from above class


class BaseListGETController(Resource):
    method_decorators = [jwt_required]

    def get(self, ModelClass, **dictionary):
        try:
            data = {}
            if request.get_json() is None:
                data = ModelClass.get(dictionary)
            else:
                data = ModelClass.get({**dictionary, **request.get_json()})

            if type(data) is list:
                json = []
                for d in data:
                    if isinstance(d, dict):
                        json.append(d)
                    else:
                        json.append(d.to_json())
                return json
            elif isinstance(data, db.Model):
                return data.to_json()
            else:
                json = []
                for row in data:
                    dicts = {}
                    for key, value in row.items():
                        if type(value) is datetime.datetime and value is not None:
                            dicts[key] = value.strftime("%d %B %Y %H:%M")
                        else:
                            dicts[key] = value
                    json.append(dicts)
                return json
        except pymysql.Error as err:
            abort(500, message=err.args)


class BaseListPOSTController(Resource):
    method_decorators = [jwt_required]

    def post(self, ModelClass):
        try:
            json = request.get_json()
            json['created_by'] = utils.get_user_id(token=request.headers.get('token'))
            ModelClass.post(json)
            return {'message': f'{ModelClass.__tablename__} created'}, 201
        except TypeError as err:
            abort(406, message="Require data with media-type application/json")
        except KeyError as err:
            abort(406, message=f"Key {err.args} is required")
        except DataError as err:
            abort(406, message=err.args)
        except Error as err:
            abort(406, message="Invalid Base64")
        except Exception as err:
            abort(500, message=err.args)


class BaseDELETEController(Resource):
    method_decorators = [jwt_required]

    def delete(self, ModelClass, id):
        json = {'id': id}
        json['deleted_by'] = utils.get_user_id(token=request.headers.get('token'))
        ModelClass.delete(json)
        return {'message': f'{ModelClass.__tablename__} deleted'}
