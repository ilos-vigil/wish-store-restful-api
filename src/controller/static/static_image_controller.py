from flask import send_from_directory
from flask_restful import Resource, abort


class StaticImageController(Resource):
    def get(self, model, file_name):
        try:
            return send_from_directory(f'static/image/{model}', file_name)
        except Exception as err:
            abort(404, message=err.args)
