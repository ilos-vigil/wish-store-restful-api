from flask_restful import Resource


class AboutController(Resource):
    def get(self):
        return {
            'message': 'Wish Store RESTful API'
        }
