import utils
from flask_jwt_extended import jwt_required
from flask_restful import request, Resource
from operator import itemgetter
from model import Post, Comment, CommentReply, UserFriend


class FeedController(Resource):
    method_decorators = [jwt_required]

    def get(self):
        id_user = utils.get_user_id(token=request.headers.get('token'))
        user_friend = UserFriend.get({
            'id_user': id_user,
            'is_accepted': 1
        })

        user_friends_id = []
        for uf in user_friend:
            user_friends_id.append(uf.to_json()['id_target'])
        user_friends_id = tuple(user_friends_id)

        post = Post.get({
            'user_friends_id': user_friends_id
        })
        comment = Comment.get({
            'user_friends_id': user_friends_id
        })
        comment_reply = CommentReply.get({
            'user_friends_id': user_friends_id
        })

        post_data = []
        comment_data = []
        comment_reply_data = []
        for p in post:
            post_data.append(p.to_json())
        for c in comment:
            comment_data.append(c.to_json())
        for cr in comment_reply:
            comment_reply_data.append(cr.to_json())

        feed_data = {
            'post': post_data,
            'comment': comment_data,
            'comment_reply': comment_reply_data
        }

        return feed_data
