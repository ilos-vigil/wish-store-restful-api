import utils
from flask_restful import abort, request
from controller import BaseController, BaseListController
from model import Wishlist


class WishlistController(BaseController):
    def get(self, id):
        return super().get(Wishlist, id=id)

    def patch(self, id):
        return super().patch(Wishlist, id)

    def delete(self, id):
        return super().delete(Wishlist, id)


class WishlistListController(BaseListController):
    def get(self):
        return super().get(Wishlist)

    def post(self):
        return super().post(Wishlist)
