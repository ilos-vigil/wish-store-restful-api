from controller import BaseController, BaseListController, BaseListGETController
from model import UserReview


class UserReviewController(BaseController):
    def get(self, id):
        return super().get(UserReview, id=id)

    def patch(self, id):
        return super().patch(UserReview, id)

    def delete(self, id):
        return super().delete(UserReview, id)


class UserReviewListController(BaseListController):
    def get(self):
        return super().get(UserReview)

    def post(self):
        return super().post(UserReview)


class EachUserReviewController(BaseListGETController):
    def get(self, id_target):
        return super().get(UserReview, id_target=id_target)
