from controller import BaseController, BaseListController, BaseListGETController
from model import Chat


class ChatController(BaseController):
    def get(self, id):
        return super().get(Chat, id=id)

    def patch(self, id):
        return super().patch(Chat, id)

    def delete(self, id):
        return super().delete(Chat, id)


class ChatListController(BaseListController):
    def get(self):
        return super().get(Chat)

    def post(self):
        return super().post(Chat)


class ChatRecentController(BaseListGETController):
    def get(self, id_user_a, id_user_b):
        return super().get(Chat, {'id_user_a': id_user_a, 'id_user_b': id_user_b, 'type': 'recent'})


class ChatHistoryController(BaseListGETController):
    def get(self, id_user_a, id_user_b):
        return super().get(Chat, {'id_user_a': id_user_a, 'id_user_b': id_user_b, 'type': 'history'})
