from controller import BaseController, BaseListController
from model import Category


class CategoryController(BaseController):
    def get(self, id):
        return super().get(Category, id=id)

    def patch(self, id):
        return super().patch(Category, id)

    def delete(self, id):
        return super().delete(Category, id)


class CategoryListController(BaseListController):
    def get(self):
        return super().get(Category)

    def post(self):
        return super().post(Category)
