from flask_restful import abort
from controller import BaseController, BaseListController, BaseListGETController
from model import Comment


class CommentController(BaseController):
    def get(self, id):
        return super().get(Comment, id=id)

    def patch(self, id):
        return super().patch(Comment, id)

    def delete(self, id):
        return super().delete(Comment, id)


class CommentListController(BaseListController):
    def get(self):
        return super().get(Comment)

    def post(self):
        return super().post(Comment)


class CommentPostListController(BaseListGETController):
    def get(self, id_post):
        return super().get(Comment, {'id_post': id_post})
