from controller import BaseController, BaseListController
from model import UserBlock


class UserBlockController(BaseController):
    def get(self, id):
        return super().get(UserBlock, id=id)

    def patch(self, id):
        return super().patch(UserBlock, id)

    def delete(self, id):
        return super().delete(UserBlock, id)


class UserBlockListController(BaseListController):
    def get(self):
        return super().get(UserBlock)

    def post(self):
        return super().post(UserBlock)
