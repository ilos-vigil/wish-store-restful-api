from controller import BaseController, BaseListController
from model import UserFriend


class UserFriendController(BaseController):
    def get(self, id):
        return super().get(UserFriend, id=id)

    def patch(self, id):
        return super().patch(UserFriend, id)

    def delete(self, id):
        return super().delete(UserFriend, id)


class UserFriendListController(BaseListController):
    def get(self):
        return super().get(UserFriend)

    def post(self):
        return super().post(UserFriend)
