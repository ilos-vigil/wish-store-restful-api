from controller import BaseController, BaseListController
from model import Deal


class DealController(BaseController):
    def get(self, id):
        return super().get(Deal, id=id)

    def patch(self, id):
        return super().patch(Deal, id)

    def delete(self, id):
        return super().delete(Deal, id)


class DealListController(BaseListController):
    def get(self):
        return super().get(Deal)

    def post(self):
        return super().post(Deal)
