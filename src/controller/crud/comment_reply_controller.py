from controller import BaseController, BaseListController
from model import CommentReply


class CommentReplyController(BaseController):
    def get(self, id):
        return super().get(CommentReply, id=id)

    def patch(self, id):
        return super().patch(CommentReply, id)

    def delete(self, id):
        return super().delete(CommentReply, id)


class CommentReplyListController(BaseListController):
    def get(self):
        return super().get(CommentReply)

    def post(self):
        return super().post(CommentReply)
