from controller import BaseController, BaseListController
from model import ReportUser


class ReportUserController(BaseController):
    def get(self, id):
        return super().get(ReportUser, id=id)

    def patch(self, id):
        return super().patch(ReportUser, id)

    def delete(self, id):
        return super().delete(ReportUser, id)


class ReportUserListController(BaseListController):
    def get(self):
        return super().get(ReportUser)

    def post(self):
        return super().post(ReportUser)
