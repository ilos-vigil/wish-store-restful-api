from controller import BaseController, BaseListController
from model import Trans


class TransController(BaseController):
    def get(self, id):
        return super().get(Trans, id=id)

    def patch(self, id):
        return super().patch(Trans, id)

    def delete(self, id):
        return super().delete(Trans, id)


class TransListController(BaseListController):
    def get(self):
        return super().get(Trans)

    def post(self):
        return super().post(Trans)
