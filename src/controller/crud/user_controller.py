from controller import BaseController, BaseListController
from model import User


class UserController(BaseController):
    def get(self, id):
        return super().get(User, id=id)

    def patch(self, id):
        return super().patch(User, id)

    def delete(self, id):
        return super().delete(User, id)


class UserListController(BaseListController):
    def get(self):
        return super().get(User)

    def post(self):
        return super().post(User)
