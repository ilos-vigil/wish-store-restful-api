from controller import BaseController, BaseListController
from model import ReportPost


class ReportPostController(BaseController):
    def get(self, id):
        return super().get(ReportPost, id=id)

    def patch(self, id):
        return super().patch(ReportPost, id)

    def delete(self, id):
        return super().delete(ReportPost, id)


class ReportPostListController(BaseListController):
    def get(self):
        return super().get(ReportPost)

    def post(self):
        return super().post(ReportPost)
