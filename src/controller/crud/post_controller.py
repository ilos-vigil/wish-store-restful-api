from flask import abort, request
from controller import BaseController, BaseListController, BaseListGETController
from model import Post
import utils

class PostController(BaseController):
    def get(self, id):
        return super().get(Post, id=id)

    def patch(self, id):
        return super().patch(Post, id)

    def delete(self, id):
        return super().delete(Post, id)


class PostListController(BaseListController):
    def get(self):
        return super().get(Post)

    def post(self):
        return super().post(Post)


class PostUserListController(BaseListGETController):
    def get(self, id_user):
        return super().get(Post, id_user=id_user, on_wishlist=False)


class PostPaginationController(BaseListController):
    def get(self, page_number, keyword):
        try:
            print(request.get_json())
            json = request.get_json() if request.get_json() is not None else {}
            json['page_number'] = page_number
            json['keyword'] = '' if keyword == '$' else keyword
            id_user = utils.get_user_id(token=request.headers.get('token'))
            data = Post.get(json)

            if type(data) is list:
                json = []
                for d in data:
                    json.append(d.to_json_is_wishlist(id_user))
                return json
        except Exception as err:
            abort(404, message=err.args)


class PostWishlistedController(BaseListGETController):
    def get(self, id_user):
        return super().get(Post, {'id_user': id_user, 'on_wishlist': True})


class PostListUserController(BaseListGETController):
    def get(self, id_user):
        args = request.args

        if 'status' in args:
            return super().get(Post, {'id_user': id_user, 'on_wishlist': False, 'status': args['status']})
        else:
            return super().get(Post, {'id_user': id_user, 'on_wishlist': False})
