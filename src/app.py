from flask import Flask
from flask_restful import Api
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from model import db
from model.seed import seed_all
from error import errors
import route

app = Flask(__name__)
app.config.from_object('config.Development')
api = Api(app, errors=errors)
api = route.init_app(api)
jwt = JWTManager(app)
migrate = Migrate(app, db)
db.init_app(app)

@app.before_first_request
def create_tables():
    db.create_all()


@app.cli.command('seed')
def cli_seed():
    db.init_app(app)
    db.create_all()
    seed_all()


if __name__ == "__main__":
    app.run()
