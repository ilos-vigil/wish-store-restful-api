# Note

* Current version : 0.19.1

## Change Log

### 0.19.2

* Remove check_authentication decorator
* Add error.py to handle most errors
* Remove unused controller
* Use flasgger for documentation
* Change routes

| Old route                                               | New route                                       | Note                               |
| ------------------------------------------------------- | ----------------------------------------------- | ---------------------------------- |
| `/block/<int:id_target>`                                | -                                               | Use POST `/userblock/`             |
| `/unblock/<int:id_target>`                              | `/userblock/user_target/<int:id_user_target>`   | Obtain `id_user` by Token          |
| `/deal/user/sent`                                       | -                                               | Use GET `/deal`                    |
| `/deal/user/received`                                   | -                                               | Use GET `/deal`                    |
| `/friend/list/pending`                                  | -                                               | Use GET `/userfriend`              |
| `/friend/list/accepted`                                 | -                                               | Use GET `/userfriend`              |
| `/friend/<int:id_target>`                               | -                                               | Use POST `/userfriend`             |
| `/unfriend/<int:id_target>`                             | -                                               | Use DELETE `/userfriend`           |
| `/wishlist/user`                                        | -                                               | Use GET `/wishlist`                |
| `/trans/user/outgoing`                                  | `/trans/outgoing`                               | No longer user specific            |
| `/trans/user/incoming`                                  | `/trans/incoming`                               | No longer user specific            |
| `/userreview/user/<int:id_target>`                      | `/userreview/user/<int:id_target>`              | Change directory from user to crud |
| `/wish/<int:id_post>`                                   | -                                               | Use GET `/wishlist`                |
| `/unwish/<int:id_post>`                                 | -                                               | Use DELETE `/wishlist`             |
| `/post/page/<int:page_number>/<string:keyword>`         | `/post/page/<int:page_number>/<string:keyword>` | Change directory from user to crud |
| `/post/user`                                            | `/user/<int:id_user>/post/`                     |
| `/post/wishlist`                                        | `/user/<int:id_user>/post/wishlist`             | Change directory from user to crud |
| `/chat/history/<int:id_logon_user>/<int:id_other_user>` | `/chat/`                                        |
| `/chat/recent/<int:id_user>`                            | `/chat/recent/<int:id_user_a>/<int:id_user_b>`  | Change directory from user to crud |
| `/chat/history/<int:id_logon_user>/<int:id_other_user>` | `/chat/history/<int:id_user_a>/<int:id_user_b>` | Change directory from user to crud |

### 0.19.1

* Change handle exception
* Refactor controller
* Use JWT/Bearer Token for Authentication

### 0.19

* Change folder structure
* Adding documentation
* Refactor model
* Refactor route

### 0.18.7

* PATCH Category now can update image using base64

### 0.18.6

* Add relationship between UserFriend id_user and User

### 0.18.5

* Feed only return data from friend with status is_accpeted = 1
* Replace key `id_user` with `id_target` on FriendPendingListController
* FriendAcceptedListController now use key id_user_target (from token), query id_user or id_target == id_user_target
* Fix GET UserTarget key `user_target` return `user_user.json_min()` rather than `user_target.json_min()`
* Replace strtime format `%Y-%m-%d %H:%M:%S` to `"%d %B %Y %H:%M"`

### 0.18.4

* Set default image link on GET method if url_photo is None
* Add route `/post/user/<int:id_user>`

### 0.18.3

* POST on `/userreview` now don't need key `id_user` (value same with column `created_by` now)
* Replace route `/userreview/user/<int:id_user>` with `/userreview/user/<int:id_target>`
* Route `/post/page/<int:page_number>/<string:keyword>` and `/post` now only show post with status 0 (active)
* Add route `/friend/list/accepted` and `/friend/list/pending`
* Add relationship between UserFriend id_target and User

### 0.18.2

* Add route `/userreview/user/<int:id_user>` and it's controller

### 0.18.1

* Change GET `/feed` JSON model return

### 0.18

* Add `/feed` route
* Add FeedController
* GET user's friend of log-on user
* GET user's friend Post, Comment & CommentReply for `/feed` route

### 0.17.12

* Add column `status` to model User

### 0.17.11

* Add relation between model Trans and Deal

### 0.17.10

* Update User's seed
* GET model ReportUser return user detail

### 0.17.9

* Add column no_rekening on model User (required on POST method)

### 0.17.8

* Method GET ReportPost now return user's of reported post

### 0.17.7

* Set default 0 to Trans's status
* Rename route `/trans/user` to `/trans/user/outgoing`
* Add route `/trans/user/incoming`
* Seperate CRUD and non-CRUD Trans controller

### 0.17.6

* Add relation between ReportPost and User/Post
* Fix decorator HTTP Status Code

### 0.17.5

* Allow update Trans's url_photo with Base64

### 0.17.4

* Update Deal's status to 1 when Trans is inserted
* Allow update Trans's status

### 0.17.3

* Add route `/deal/user/sent` & `/deal/user/received`  

### 0.17.2

* Route `/post/user` now accept param `status`
* Add unique constraint to model UserFriend, UserBlock & Wishlist
* Method DELETE is hard delete now on model UserFriend & UserBlock

### 0.17.1

* Replace model ReportDeal with ReportPost

### 0.17

* Seperate controller by only CRUD or not
* Add route `/block`, `/unblock`, `/friend`, `/unfriend`. Update necessary Model
* Post Pagination now with keyword
* Update seed
* Add column `kota` & `trans` to model Trans
* Make few key on POST become optional
* Add statistic wishlist count, comment count & user average rating on model Post

### 0.16

* Replace `.to_json()` with `.json_min()` for key `user` in model Comment & CommentReply
* Add key `is_wishlist` on post pagination
* Add column `status` on model post
* Add relationship User and Post on model Deal
* Add routes to GET data created-by log-on user

### 0.15.5

* Create relationship between model User and Comment, CommentReply

### 0.15.4

* Seperate CRUD and non-CRUD route
* Method GET Comment now return list of CommentReply
* Delete unused column in model Wishlist
* Add column `id_onesignal` to model User
* Add route `/comment/post/<int:id_post>` to get list of comment from specific post

### 0.15.3

* Model post, add relation between column created_by & model user

### 0.15.2

* Add `/wish/<int:id_post>` and `/unwish/<int:id_post>` routes
* DELETE method on model Wishlist now is hard delete

### 0.15.1

* Add User seeder

### 0.15

* Fix inheritance to all controller
* Major refactoring
* GET last chat from log-on user now use ORM and return user details

### 0.14.4

* GET post on wishlist support

### 0.14.3

* Fix response on GET method for model with column `url_photo`

### 0.14.2

* Add routing to get last chat from log-on user and chat history between 2 user

### 0.14.1

* Use `pathlib` library to ensure cross-platform compatibility

### 0.14

* Update .gitignore
* Add relationship between chat and user
* Fix critical error on GET method all items
* Add seeder from JSON file
* Move seeder function location to model.seed
* Modify data type of column `deskripsi` on model post

### 0.13.6

* Fix GET last chat for log-on user by fix `BaseListController`

### 0.13.5

* Add pagination for model post

### 0.13.4

* Load configuration from python module

### 0.13.3

* Fix `url_photo` not removed on model and CommentReply (POST and PATCH method)

### 0.13.2

* Remove `url_photo` column from model Comment and CommentReply
* Use SQL query to get last chat from log-on user
* Get chat history between 2 user
* Refactoring GET method

### 0.13.1

* Fix `/register` POST method
* Make some JSON key optional on user model POST

### 0.13

* Create relationship (without Foreign Key) on model `post`
* Fix incorrect `id_[a-z]` column data type

### 0.12.4

* Replace key `msg` with `message` on BaseController and BaseListController
* Partial Refactoring **model**, replacing multiple args with single arg. (dictionary) on `__init__`

### 0.12.3

* Replace timestamp with string DateTime on GET method

### 0.12.2

* Add message key on `/register` and `/login` route

### 0.12.1

* Add seed feature, use `flask seed` to seed database

### 0.12

* Refactoring **controller**
* Automatically assign value of column `created_by` on POST method and `deleted_by` on DELETE method
* Get user id with token/username from `wishfunction` helper

### 0.11

* Add index to some columns on model `user`
* Generate token when user login for first time
* Fix HTTP code on some route/method
* Create decorator to verify token
* All method now require token on header request, except route `/login`, `/register` and `/static/image/<string:model>/<string:file_name>`
* Replace `return` on exception with `abort()`

### 0.10.3

* Add `chat` model, controller, route

### 0.10.2

* Add `category` model, controller, route

### 0.10.1

* Fix `trans` directory missing on POST `/trans`
* Fix 500 on GET `/commentreply`
* Fix can GET single deleted item

### 0.10

* Change model, controller & route name
  * `CommentHeader` -> `Comment`
  * `Comment` -> `CommentReply`
  * `HTrans` -> `Trans`
* Add `created_by` and `deleted_by` to all column
* Prevent soft deleted item obtained/changed from GET and PATCH method
* Change message when attempt to delete non-exist or soft-deleted item, with `wishfunction` helper
* Try/except if attempt to PATCH/DELETE non-exist item (partially working)

### 0.9.2

* Fix typo route name (`/commmentheader` to `commentheader`)
* Hard-code domain path

### 0.9.1

* Fix .gitignore rule on `static/image` folder

### 0.9

* Support image uplaod to model with `base64` JSON key
* Add static image controller
* Return message on POST, PATCH and DELETE method when no error occured
* Add `wishfunction` module
* `url_photo` JSON key not required & ignored on POST method

### 0.8.3

* Fix wrong path for image upload

### 0.8.2

* Change image controller POST accepted data JSON
* Write image in base64 format to file

### 0.8.1

* Update docs
* Fix can't get single item

### 0.8

* Add image model, controller & route for testing upload image (will be removed)
* Add register/login controller & route

### 0.7

* Change file/folder structure
* Add new models
* Add columns (`created_by`, `created_at`, `deleted_at`, `deleted_by`) to some models
* Rename all model, controller, route, module into singular
* Return correct HTTP code to all request type
* Update .gitignore

## Docs

### To Do

1. **Change column data type**
2. **Change some column into foreign key or relationship**
3. Advance Authentication/security
4. **Refactoring**
5. Add Seeder
6. Add important script (e.g. write Base64 to file) to a module
7. Correct Exception on GET/PATCH/DELETE if non-exist `id` is present

### Files

* app.py
* controller.py
* model.py

### Routing (CRUD)

| Endpoint                   | Methods            | Rule                        |
| -------------------------- | ------------------ | --------------------------- |
| categorylistcontroller     | GET, POST          | /category                   |
| categorycontroller         | DELETE, GET, PATCH | /category/<int:id>          |
| chatlistcontroller         | GET, POST          | /chat                       |
| chatcontroller             | DELETE, GET, PATCH | /chat/<int:id>              |
| commentlistcontroller      | GET, POST          | /comment                    |
| commentcontroller          | DELETE, GET, PATCH | /comment/<int:id>           |
| commentpostlistcontroller  | GET                | /comment/post/<int:id_post> |
| commentreplylistcontroller | GET, POST          | /commentreply               |
| commentreplycontroller     | DELETE, GET, PATCH | /commentreply/<int:id>      |
| deallistcontroller         | GET, POST          | /deal                       |
| dealcontroller             | DELETE, GET, PATCH | /deal/<int:id>              |
| postlistcontroller         | GET, POST          | /post                       |
| postcontroller             | DELETE, GET, PATCH | /post/<int:id>              |
| postuserlistcontroller     | GET                | /post/user/<int:id_user>    |
| reportpostlistcontroller   | GET, POST          | /reportpost                 |
| reportpostcontroller       | DELETE, GET, PATCH | /reportpost/<int:id>        |
| reportuserlistcontroller   | GET, POST          | /reportuser                 |
| reportusercontroller       | DELETE, GET, PATCH | /reportuser/<int:id>        |
| translistcontroller        | GET, POST          | /trans                      |
| transcontroller            | DELETE, GET, PATCH | /trans/<int:id>             |
| userblocklistcontroller    | GET, POST          | /userblock                  |
| userblockcontroller        | DELETE, GET, PATCH | /userblock/<int:id>         |
| userfriendlistcontroller   | GET, POST          | /userfriend                 |
| userfriendcontroller       | DELETE, GET, PATCH | /userfriend/<int:id>        |
| userlistcontroller         | GET, POST          | /user                       |
| usercontroller             | DELETE, GET, PATCH | /user/<int:id>              |
| userreviewlistcontroller   | GET, POST          | /userreview                 |
| userreviewcontroller       | DELETE, GET, PATCH | /userreview/<int:id>        |
| wishlistlistcontroller     | GET, POST          | /wishlist                   |
| wishlistcontroller         | DELETE, GET, PATCH | /wishlist/<int:id>          |

### Routing (User)

| Endpoint                     | Methods   | Rule                                                  |
| ---------------------------- | --------- | ----------------------------------------------------- |
| aboutcontroller              | GET       | /                                                     |
| feedcontroller               | GET       | /feed                                                 |
| chatrecentcontroller         | GET       | /chat/recent/<int:id_user>                            |
| chathistorycontroller        | GET       | /chat/history/<int:id_logon_user>/<int:id_other_user> |
| postlistusercontroller       | GET       | /post/user                                            |
| postwishlistedcontroller     | GET       | /post/wishlist                                        |
| postpaginationcontroller     | GET, POST | /post/page/<int:page_number>/<string:keyword>         |
| wishcontroller               | GET       | /wish/<int:id_post>                                   |
| unwishcontroller             | GET       | /unwish/<int:id_post>                                 |
| translistincomingcontroller  | GET       | /trans/user/incoming                                  |
| translistoutgoingcontroller  | GET       | /trans/user/outgoing                                  |
| wishlistlistusercontroller   | GET       | /wishlist/user                                        |
| friendcontroller             | GET       | /friend/<int:id_target>                               |
| unfriendcontroller           | GET       | /unfriend/<int:id_target>                             |
| friendacceptedlistcontroller | GET       | /friend/list/accepted                                 |
| friendpendinglistcontroller  | GET       | /friend/list/pending                                  |
| blockcontroller              | GET       | /block/<int:id_target>                                |
| unblockcontroller            | GET       | /unblock/<int:id_target>                              |
| sentdealcontroller           | GET       | /deal/user/sent                                       |
| receiveddealcontroller       | GET       | /deal/user/received                                   |
| eachuserreviewcontroller     | GET       | /userreview/user/<int:id_target>                      |

### Routing (Other)

| Endpoint              | Methods | Rule                                            |
| --------------------- | ------- | ----------------------------------------------- |
| logincontroller       | POST    | /login                                          |
| registercontroller    | POST    | /register                                       |
| static                | GET     | /static/<path:filename>                         |
| staticimagecontroller | GET     | /static/image/<string:model>/<string:file_name> |

### HTTP Method

| HTTP Verb | Plural | Singular |
| --------- | ------ | -------- |
| GET       | Yes    | Yes      |
| POST      | Yes    | No       |
| PUT       | No     | No       |
| PATCH     | No     | Yes      |
| DELETE    | No     | Yes*     |

## Testing example

### Plural GET (Read)

* Command

```
curl -X GET http://localhost:5000/user
```

* Output

```json
[
    {
        "id": 1,
        "nama": "Andi",
        "gender": true,
        "username": "ABC",
        "password": "Test123",
        "token": null,
        "isadmin": false,
        "alamat": "XYZ",
        "kota": "ABC",
        "created_at": 1572802011.0,
        "deleted_at": 1572802012.0,
        "is_deleted": false
    },
    {
        "id": 2,
        "nama": "Budi",
        "gender": true,
        "username": "Budi123",
        "password": "Test123",
        "token": null,
        "isadmin": false,
        "alamat": "Ngagel 1",
        "kota": "Surabaya",
        "created_at": 1573048620.0,
        "deleted_at": null,
        "is_deleted": false
    }
]
```

### Singular GET (Read)

* Command

```
curl -X GET http://localhost:5000/user/1
```

* Output

```json
{
    "id": 1,
    "nama": "Andi",
    "gender": true,
    "username": "ABC",
    "password": "Test123",
    "token": null,
    "isadmin": false,
    "alamat": "XYZ",
    "kota": "ABC",
    "created_at": 1572802011.0,
    "deleted_at": 1572802012.0,
    "is_deleted": false
}
```

### POST (Create)

* Command

```
curl -X POST -H "Content-Type: application/json" -d @user_post.json http://localhost:5000/user
```

* Input format

```json
// user_post.json
{
    "nama": "Budi",
    "gender": 1,
    "username": "Budi123",
    "password": "Test123",
    "token": null,
    "isadmin": 0,
    "alamat": "Ngagel 1",
    "kota": "Surabaya"
}
```

### PATCH (Update)

* Command
  
```
curl -X PATCH -H "Content-Type: application/json" -d @user_patch.json http://localhost:5000/user
```

* Input example

```json
{
    "alamat": "Ngagel 2"
}
```

* Data before PATCH

```json
{
    "id": 2,
    "nama": "Budi",
    "gender": true,
    "username": "Budi123",
    "password": "Test123",
    "token": null,
    "isadmin": false,
    "alamat": "Ngagel 1",
    "kota": "Surabaya",
    "created_at": 1573048620.0,
    "deleted_at": null,
    "is_deleted": false
}
```

* Data after PATCH

```json
{
    "id": 2,
    "nama": "Budi",
    "gender": true,
    "username": "Budi123",
    "password": "Test123",
    "token": null,
    "isadmin": false,
    "alamat": "Ngagel 2",
    "kota": "Surabaya",
    "created_at": 1573048620.0,
    "deleted_at": null,
    "is_deleted": false
}
```

### DELETE (Delete)

* Command

```
curl -X DELETE http://localhost:5000/user/1
```

* Data before DELETE

```json
{
    "id": 1,
    "nama": "Andi",
    "gender": true,
    "username": "ABC",
    "password": "Test123",
    "token": null,
    "isadmin": false,
    "alamat": "XYZ",
    "kota": "ABC",
    "created_at": 1572802011.0,
    "deleted_at": null,
    "is_deleted": false
}
```

* Data after DELETE

```json
{
    "id": 1,
    "nama": "Andi",
    "gender": true,
    "username": "ABC",
    "password": "Test123",
    "token": null,
    "isadmin": false,
    "alamat": "XYZ",
    "kota": "ABC",
    "created_at": 1572802011.0,
    "deleted_at": 1573049104.0,
    "is_deleted": true
}
```

## Guide

### Virtual Environment configuration (Windows)

```
python -m venv venv
.\venv\Scripts\activate.bat
pip install --user --requirement requirements.txt
```

### Virtual Environment configuration (Linux)

* Configure & go to env.

```
python -m venv venv
source venv/bin/activate
pip install --user --requirement requirements.txt
```

* Quit from env.

```
deactivate
```

### Create requirement

```
pip install flask-restful flask-migrate flask-sqlalchemy
pip freeze > requirements.txt
```

### Prepare MySQL

```
mysql -u 'root'
CREATE DATABASE flask;
create user 'flask'@'localhost' identified by 'Test123!';
GRANT ALL PRIVILEGES ON flask.* TO 'flask'@'localhost';
COMMIT;
```

### Migration

> https://flask-migrate.readthedocs.io/en/latest/

```
flask db init
flask db migrate
flask db upgrade
```

### Testing with `curl`

```
curl -X GET -H "Content-Type: application/json" -d @data.json http://localhost:5000/user
```

## External guide & reference

* https://www.restapitutorial.com/lessons/httpmethods.html
* https://stackoverflow.com/questions/37806625/sqlalchemy-create-relations-but-without-foreign-key-constraint-in-db
* https://stackoverflow.com/questions/17972020/how-to-execute-raw-sql-in-flask-sqlalchemy-app
* https://stackoverflow.com/questions/20743806/sqlalchemy-execute-return-resultproxy-as-tuple-not-dict/54753785
