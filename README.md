# Wish Store RESTful API

RESTful API backend for [Wish Store Mobile](https://gitlab.com/Lungaville/wish-store-mobile) and [Admin Wish Store](https://gitlab.com/infinite9d/admin-wishstore)

## Features

* Bearer Token Authentication
* CRUD (GET, POST, PATCH, DELETE) operation for all tables
* Swagger UI for documentation

## Documentation

All documentation can be found at [DOCS.md](./DOCS.md)

## Resources

* [Flask](https://flask.palletsprojects.com/)
* [Flask-Bcrypt](https://flask-bcrypt.readthedocs.io/en/latest/)
* [Flask-JWT-Extended](https://flask-jwt-extended.readthedocs.io/en/stable/)
* [Flask-RESTful](http://flask-restful.readthedocs.io/)
* [Flask-SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/)
* [Flask-Migrate](https://flask-migrate.readthedocs.io/en/latest/)
* [SQLAlchemy](https://www.sqlalchemy.org/)
